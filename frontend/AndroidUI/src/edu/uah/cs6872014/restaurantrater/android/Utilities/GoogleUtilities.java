/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.android.Utilities;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.HttpResponse;

/**
 * @author shreedhan
 * Utilities to Communicate with google place server
 */
public class GoogleUtilities {

	@SuppressWarnings("deprecation")
	public static String getPlacesJSONString(String place) {
		try {
			place = URLEncoder.encode(place, "UTF-8").replaceAll("\\+", "%20");
		}
		catch (UnsupportedEncodingException e) {
			place = URLEncoder.encode(place).replaceAll("\\+", "%20");
			e.printStackTrace();
		}
		String url = Constants.GOOGLE_PLACE_URL + place;
		HttpResponse response = HTTPUtilities.get(url);
		if (response != null)
			return HTTPUtilities.getString(response.getEntity());
		else
			return null;
	}

	public static String getPlacesLocationJSONString(String location) {
		String url = Constants.GOOGLE_LOCATION_URL + location;
		HttpResponse response = HTTPUtilities.get(url);
		if (response != null)
			return HTTPUtilities.getString(response.getEntity());
		else
			return null;
	}

	public static String getPlaceDetailJSONString(String reference) {
		String url = Constants.GOOGLE_PLACE_DETAIL_URL + reference;
		HttpResponse response = HTTPUtilities.get(url);
		if (response != null)
			return HTTPUtilities.getString(response.getEntity());
		else
			return null;
	}

	public static String getSinglePlaceJSONString(double lat, double lng) {
		String url = Constants.GOOGLE_SINGLE_PLACE_URL + lat + "," + lng;
		HttpResponse response = HTTPUtilities.get(url);
		if (response != null)
			return HTTPUtilities.getString(response.getEntity());
		else
			return null;
	}
}
