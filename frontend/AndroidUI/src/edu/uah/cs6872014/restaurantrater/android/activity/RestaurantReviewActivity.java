package edu.uah.cs6872014.restaurantrater.android.activity;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import edu.uah.cs6872014.restaurantrater.android.R;
import edu.uah.cs6872014.restaurantrater.android.Utilities.RESTUtilities;
import edu.uah.cs6872014.restaurantrater.android.Utilities.SessionManager;
import edu.uah.cs6872014.restaurantrater.android.model.Category;
import edu.uah.cs6872014.restaurantrater.android.model.Restaurant;

/**
 * @author Madhu Sigdel Activity which displays a restaurant review screen to
 *         the user to add/update/delete comments and rating.
 * 
 */
public class RestaurantReviewActivity extends Activity {

	private HashMap<Integer, Category> categories;
	private Button submitButton;
	private Button deleteButton;
	private String commentText;
	private EditText commentEditText;
	private int countCategory;
	private int rid;
	private String gid;
	private int uid;
	private String staddress;
	private String name;
	private int zipcode;
	private SessionManager session;
	private double latitude;
	private double longitude;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_restaurant_review);

		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		fetchAllCategories();
		this.countCategory = getCountCategoryFromDB();
		Intent intent = getIntent();
		this.rid = intent.getIntExtra("rid", 0);
		this.gid = intent.getStringExtra("gid");
		this.staddress = intent.getStringExtra("staddress");
		this.name = intent.getStringExtra("name");
		this.zipcode = intent.getIntExtra("zipcode", 0);
		this.latitude = intent.getDoubleExtra("latitude", 0);
		this.longitude = intent.getDoubleExtra("longitude", 0);

		session = new SessionManager(getApplicationContext());
		HashMap<String, String> user = session.getUserDetails();

		if (user.get("uid") != null) {
			uid = Integer.parseInt(user.get("uid"));
		} else {
			finish();
		}

		final RatingBar[] ratingBar = new RatingBar[countCategory];

		TextView restaurant_name = (TextView) findViewById(R.id.restaurant_name);
		restaurant_name.setText(name);
		TextView restaurant_address = (TextView) findViewById(R.id.staddress);
		restaurant_address.setText(staddress);

		for (int count = 1; count <= this.countCategory; count++) {
			ratingBar[count - 1] = new RatingBar(this, null);
			ratingBar[count - 1].setIsIndicator(false);
			ratingBar[count - 1].setStepSize((float) 1);

			int rating = RESTUtilities.fetchUserRestaurantCategoryRating(uid,
					rid, count);
			ratingBar[count - 1].setRating(rating);

			TextView tvCategory = new TextView(this);
			Category category = categories.get(count);
			tvCategory.setText(category.getName());
			//
			View vGrating = findViewById(R.id.rating_layout);
			((ViewGroup) vGrating).addView(tvCategory, new LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT));

			((ViewGroup) vGrating).addView(ratingBar[count - 1],
					new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
							ViewGroup.LayoutParams.WRAP_CONTENT));

		}

		commentEditText = (EditText) findViewById(R.id.review_comment);
		String fetchComment = RESTUtilities.fetchUserRestaurantComments(uid,
				rid);
		if (fetchComment != null) {
			try {
				JSONObject jsonComment = new JSONObject(fetchComment);
				commentEditText.setText(jsonComment.getString("comment"));
			} catch (JSONException e) {
			}

		}

		submitButton = (Button) findViewById(R.id.submit_button);

		submitButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// New Restaurant
				if (rid == 0) {
					if (latitude >= -90 && latitude <= 90 && longitude >= -180
							&& longitude <= 180) {
						if (RESTUtilities.writeRestaurant(gid, name, staddress,
								zipcode, latitude, longitude) == null) {
							Toast.makeText(getApplicationContext(),
									"Could not update the data.",
									Toast.LENGTH_SHORT).show();
							finish();
						}
						Restaurant restaurant = fetchRestaurantDetailsFromDB(gid);
						if (restaurant == null) {
							Toast.makeText(getApplicationContext(),
									"Could not update the data.",
									Toast.LENGTH_SHORT).show();
							finish();
						}
						rid = restaurant.getRid();
						if (rid == 0) {
							Toast.makeText(getApplicationContext(),
									"Could not update the data.",
									Toast.LENGTH_SHORT).show();
							finish();
						}
					} else {
						Toast.makeText(getApplicationContext(),
								"Bad data found", Toast.LENGTH_SHORT).show();
						finish();
					}
				}

				// Comment
				commentText = commentEditText.getText().toString();
				if (RESTUtilities.writeComment(uid, rid, commentText) == null) {
					Toast.makeText(getApplicationContext(),
							"Could not update the data.", Toast.LENGTH_SHORT)
							.show();
					finish();
				}

				// Rating
				for (int count = 1; count <= countCategory; count++) {
					int star = (int) ratingBar[count - 1].getRating();
					if (star != 0 || star <= 5) {
						if (RESTUtilities.writeRating(uid, rid, count, star) == null) {
							Toast.makeText(getApplicationContext(),
									"Could not update the data.",
									Toast.LENGTH_SHORT).show();
							finish();
						}
					}
				}
				Toast.makeText(getApplicationContext(), "Successfully updated",
						Toast.LENGTH_SHORT).show();
				finish();
			}
		});

		deleteButton = (Button) findViewById(R.id.delete_button);

		deleteButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// New Restaurant
				if (rid == 0) {
					Toast.makeText(getApplicationContext(),
							"No Data to delete", Toast.LENGTH_SHORT).show();
					finish();
				}

				// Comment
				RESTUtilities.deleteComment(uid, rid);

				// Rating
				for (int count = 1; count <= countCategory; count++) {
					int star = (int) ratingBar[count - 1].getRating();
					if (star != 0) {
						if (RESTUtilities.deleteRating(uid, rid, count) == null) {
							Toast.makeText(getApplicationContext(),
									"Could not delete the data.",
									Toast.LENGTH_SHORT).show();
							finish();
						}
					}
				}
				Toast.makeText(getApplicationContext(), "Successfully Deleted",
						Toast.LENGTH_SHORT).show();
				finish();
			}
		});

	}

	private int getCountCategoryFromDB() {
		int count = 0;
		String stringCount = RESTUtilities.getCategoriesCount();
		if (stringCount == null)
			return 0;
		JSONObject jsonCount = null;
		try {
			jsonCount = new JSONObject(stringCount);
			count = jsonCount.getInt("count");
		} catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
		return count;
	}

	@SuppressLint("UseSparseArrays")
	private void fetchAllCategories() {
		String stringCategories = RESTUtilities.getAllCategories();
		if (stringCategories == null) {
			return;
		}
		try {
			JSONArray jsonCategories = new JSONArray(stringCategories);
			categories = new HashMap<Integer, Category>();
			for (int i = 0; i < jsonCategories.length(); i++) {
				String jsonCategory = jsonCategories.getString(i);
				Category category = Category.toCategory(jsonCategory);
				categories.put(category.getCid(), category);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private Restaurant fetchRestaurantDetailsFromDB(String id) {
		if (id == null)
			return null;
		String jsonRestaurant = RESTUtilities.getRestaurant(id);
		if (jsonRestaurant == null)
			return null;
		Restaurant restaurant = null;
		if (jsonRestaurant != null)
			restaurant = Restaurant.toRestaurant(jsonRestaurant);
		return restaurant;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.restaurant_review, menu);
		return true;
	}

}
