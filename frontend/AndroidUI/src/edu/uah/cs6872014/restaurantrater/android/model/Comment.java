package edu.uah.cs6872014.restaurantrater.android.model;

import java.sql.Timestamp;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Suman
 *
 */
public class Comment {

	private int			uid;
	private int			rid;
	private String		comment;
	private Timestamp	createdts;
	private Timestamp	modifiedts;

	/**
	 * @return the uid
	 */
	public int getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(int uid) {
		this.uid = uid;
	}

	/**
	 * @return the rid
	 */
	public int getRid() {
		return rid;
	}

	/**
	 * @param rid
	 *            the rid to set
	 */
	public void setRid(int rid) {
		this.rid = rid;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the createdts
	 */
	public Timestamp getCreatedts() {
		return createdts;
	}

	/**
	 * @param createdts
	 *            the createdts to set
	 */
	public void setCreatedts(Timestamp createdts) {
		this.createdts = createdts;
	}

	/**
	 * @return the modifiedts
	 */
	public Timestamp getModifiedts() {
		return modifiedts;
	}

	/**
	 * @param modifiedts
	 *            the modifiedts to set
	 */
	public void setModifiedts(Timestamp modifiedts) {
		this.modifiedts = modifiedts;
	}

	public JSONObject toJSON() {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("uid", uid).put("rid", rid).put("comment", comment).put("createdts", createdts)
				.put("modifiedts", modifiedts);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObj;
	}

	public static Comment toComment(String jsonComment) {
		Comment comment = new Comment();
		try {
			JSONObject jsonObj = new JSONObject(jsonComment);
			comment.uid = jsonObj.getInt("uid");
			comment.rid = jsonObj.getInt("rid");
			comment.comment = jsonObj.getString("comment");
			comment.createdts = Timestamp.valueOf(jsonObj.getString("createdts"));
			comment.modifiedts = Timestamp.valueOf(jsonObj.getString("modifiedts"));
		}
		catch (JSONException e) {
			System.out.println("Couldn't parse JSON for uid : " + comment.uid + " & rid : " + comment.rid);
			if (comment.uid <= 0 || comment.rid <= 0)
				return null;
		}
		return comment;
	}

}
