package edu.uah.cs6872014.restaurantrater.android.activity;

import org.apache.http.HttpResponse;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import edu.uah.cs6872014.restaurantrater.android.R;
import edu.uah.cs6872014.restaurantrater.android.Utilities.UserUtilities;
import edu.uah.cs6872014.restaurantrater.android.model.Category;

/**
 * Activity which displays a screen for adding new category to user with admin privileges.
 */

/**
 * @author Suman
 *
 */
public class CategoryActivity extends Activity {

	String					categoryName;
	String					categoryDescription;

	EditText				categoryNameView;
	EditText				categoryDescriptionView;
	Button					categoryAddButton;

	private static String	SUCCESS	= "Category added successfully!";
	private static String	FAILURE	= "Could not add category. Duplicate category name. Try again.";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category);

		categoryNameView = (EditText) findViewById(R.id.category_name);
		categoryDescriptionView = (EditText) findViewById(R.id.category_description);

		categoryAddButton = (Button) findViewById(R.id.category_add);
		categoryAddButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				addCategory();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.category, menu);
		return true;
	}

	public void addCategory() {

		// Reset errors
		categoryNameView.setError(null);

		View focusView = null;
		boolean cancel = false;

		// Get the category name and description from the EditText
		categoryName = categoryNameView.getText().toString();
		categoryDescription = categoryDescriptionView.getText().toString();

		// Check if category name is provided
		if (TextUtils.isEmpty(categoryName)) {
			categoryNameView.setError(getString(R.string.error_field_required));
			focusView = categoryNameView;
			cancel = true;
		}
		
		if (cancel) {
			focusView.requestFocus();
		}
		else {
			add();
		}
	}

	private void add() {
		Category category = new Category();
		category.setName(categoryName);
		category.setDescription(categoryDescription);

		HttpResponse response = UserUtilities.addCategory(category);
		int status = response.getStatusLine().getStatusCode();
		if (status == 200) {
			Toast.makeText(getApplicationContext(), SUCCESS, Toast.LENGTH_LONG).show();
			finish();
		}
		else {
			Toast.makeText(getApplicationContext(), FAILURE, Toast.LENGTH_LONG).show();

		}
	}
}
