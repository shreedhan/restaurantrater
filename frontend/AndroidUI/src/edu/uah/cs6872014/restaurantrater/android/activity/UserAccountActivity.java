package edu.uah.cs6872014.restaurantrater.android.activity;

import org.apache.http.HttpResponse;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import edu.uah.cs6872014.restaurantrater.android.R;
import edu.uah.cs6872014.restaurantrater.android.Utilities.HTTPUtilities;
import edu.uah.cs6872014.restaurantrater.android.Utilities.SessionManager;
import edu.uah.cs6872014.restaurantrater.android.Utilities.UserUtilities;
import edu.uah.cs6872014.restaurantrater.android.model.User;

/**
 * Activity which displays a screen showing the information of logged user.
 */

/**
 * @author Suman
 *
 */
public class UserAccountActivity extends Activity {

	private User			user	= null;

	// UI references.
	private EditText		mUsernameView;
	private EditText		mEmailView;
	private EditText		mFnameView;
	private EditText		mMnameView;
	private EditText		mLnameView;
	private EditText		mStaddressView;
	private EditText		mZipcodeView;
	private Button			mResetButton;
	private Button			mSaveButton;

	SessionManager			session;

	private static String	EMAIL	= "email";
	private static String	SUCCESS	= "Successfully updated!";
	private static String	FAILURE	= "Couldnot update your information. Please try again!";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_account);

		session = new SessionManager(getApplicationContext());

		HttpResponse response = UserUtilities.getUserData(session.getUserDetails().get(EMAIL));
		int statuscode = response.getStatusLine().getStatusCode();
		if (statuscode == 200) {
			String userString = HTTPUtilities.getString(response.getEntity());
			user = User.toUser(userString);
		}
		else {
			finish();
			return;
		}

		mUsernameView = (EditText) findViewById(R.id.edit_username);
		mEmailView = (EditText) findViewById(R.id.edit_email);
		mFnameView = (EditText) findViewById(R.id.edit_fname);
		mMnameView = (EditText) findViewById(R.id.edit_mname);
		mLnameView = (EditText) findViewById(R.id.edit_lname);
		mStaddressView = (EditText) findViewById(R.id.edit_staddress);
		mZipcodeView = (EditText) findViewById(R.id.edit_zipcode);

		// Fill the value of the user from database
		reset();

		mSaveButton = (Button) findViewById(R.id.edit_save);
		mSaveButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				updateUser();
			}
		});

		mResetButton = (Button) findViewById(R.id.edit_reset);
		mResetButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				reset();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_settings, menu);
		return true;
	}

	public void updateUser() {

		boolean cancel = false;
		View focusView = null;

		String mEmail = mEmailView.getText().toString();
		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		}
		else if (!mEmail.contains("@")) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		}

		if (cancel) {
			focusView.requestFocus();
		}
		else {
			update();
		}

	}

	public void update() {
		User mUser = new User();

		mUser.setUid(user.getUid());
		mUser.setUsername(mUsernameView.getText().toString());
		mUser.setPassword(user.getPassword());
		mUser.setEmail(mEmailView.getText().toString());
		mUser.setFname(mFnameView.getText().toString());
		mUser.setMidname(mMnameView.getText().toString());
		mUser.setLname(mLnameView.getText().toString());
		mUser.setStaddress(mStaddressView.getText().toString());
		mUser.setZipcode(Integer.parseInt(mZipcodeView.getText().toString()));
		mUser.setLastlogints(user.getLastlogints());
		mUser.setCreatedts(user.getCreatedts());

		HttpResponse response = UserUtilities.updateUser(mUser);
		int statusCode = response.getStatusLine().getStatusCode();
		if (statusCode == 200) {
			user = mUser;
			Toast.makeText(getApplicationContext(), SUCCESS, Toast.LENGTH_LONG).show();
			finish();
		}
		else {
			Toast.makeText(getApplicationContext(), FAILURE, Toast.LENGTH_LONG).show();

		}
	}

	public void reset() {
		mUsernameView.setText(user.getUsername());
		mEmailView.setText(user.getEmail());
		mFnameView.setText(user.getFname());
		mMnameView.setText(user.getMidname());
		mLnameView.setText(user.getLname());
		mStaddressView.setText(user.getStaddress());
		mZipcodeView.setText(Integer.toString(user.getZipcode()));
	}
}
