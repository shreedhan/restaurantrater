package edu.uah.cs6872014.restaurantrater.android.activity;

import org.apache.http.HttpResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import edu.uah.cs6872014.restaurantrater.android.R;
import edu.uah.cs6872014.restaurantrater.android.Utilities.HTTPUtilities;
import edu.uah.cs6872014.restaurantrater.android.Utilities.SessionManager;
import edu.uah.cs6872014.restaurantrater.android.Utilities.UserUtilities;

/**
 * @author Suman
 * 
 */
public class MainActivity extends Activity implements LocationListener {

	private String			searchKeyword;

	// UI references.
	private Button			loginButton;
	private EditText		searchKeywordView;
	private ImageButton		searchButton;
	private ImageButton		nearbySearchButton;
	private TextView		mUsernameView;

	private SessionManager	session;
	private LocationManager	locationManager;
	private String			provider;
	private double			latitude;
	private double			longitude;
	private static boolean	isLoggedIn		= false;

	private static String	UID				= "uid";
	private static String	USERNAME		= "username";
	private static String	EMAIL			= "email";
	private static String	EXIT			= "EXIT";
	private static String	ADMIN_EMAIL		= "admin@rr.com";
	private static String	NO_FAVORITES	= "No favorite restaurants";
	private static String	DATABASE_ERROR	= "Could not fetch favorites. Please try again.";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (getIntent().getBooleanExtra(EXIT, false)) {
			finish();
			return;
		}
		session = new SessionManager(getApplicationContext());
		isLoggedIn = session.isLoggedIn();

		// GPSLocation mlocListener = new GPSLocation();

		// Shreedhan - Disable strict mode for now
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		loginButton = (Button) findViewById(R.id.sign_in_button);
		loginButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, LoginActivity.class);
				startActivity(intent);
			}
		});

		mUsernameView = (TextView) findViewById(R.id.label_username);
		mUsernameView.setVisibility(View.INVISIBLE);

		searchKeywordView = (EditText) findViewById(R.id.search_keyword);
		searchButton = (ImageButton) findViewById(R.id.search_button);
		searchButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				searchKeyword = searchKeywordView.getText().toString();
				Intent intent = new Intent(MainActivity.this, SearchResultActivity.class);
				intent.putExtra("place", searchKeyword);
				startActivity(intent);
			}
		});

		nearbySearchButton = (ImageButton) findViewById(R.id.nearby_search_button);
		nearbySearchButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
				if (!enabled) {
					Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					startActivity(intent);
					Toast toast = Toast.makeText(getApplicationContext(), "Please enable location service first",
						Toast.LENGTH_LONG);
					toast.show();
					return;
				}
				String location = Double.toString(latitude) + "," + Double.toString(longitude);
				Intent intent = new Intent(MainActivity.this, SearchResultActivity.class);
				intent.putExtra("location", location);
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		MenuItem itemChangePassword = menu.findItem(R.id.action_change_password);
		itemChangePassword.setVisible(isLoggedIn);

		MenuItem itemAccount = menu.findItem(R.id.action_account);
		itemAccount.setVisible(isLoggedIn);

		MenuItem itemLogout = menu.findItem(R.id.action_logout);
		itemLogout.setVisible(isLoggedIn);

		MenuItem itemFavorites = menu.findItem(R.id.action_favorites);
		itemFavorites.setVisible(isLoggedIn);

		MenuItem itemCategory = menu.findItem(R.id.action_catogry);
		itemCategory.setVisible((isLoggedIn && isAdmin()));

		return true;
	}

	// Actions to be performed when items are selected from the action bar
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = null;
		switch (item.getItemId()) {
		case R.id.action_settings:
			session.checkLogin();
			intent = new Intent(MainActivity.this, ChangePasswordActivity.class);
			startActivity(intent);
			return true;

		case R.id.action_change_password:
			intent = new Intent(MainActivity.this, ChangePasswordActivity.class);
			startActivity(intent);
			return true;

		case R.id.action_account:
			intent = new Intent(MainActivity.this, UserAccountActivity.class);
			startActivity(intent);
			return true;

		case R.id.action_favorites:
			HttpResponse response = UserUtilities.getFavoriteRestaurants(session.getUserDetails().get(UID));
			int status = response.getStatusLine().getStatusCode();
			if (status == 200) {
				String json = HTTPUtilities.getString(response.getEntity());
				Log.d("suman", json);
				try {
					JSONArray jsonArray = new JSONArray(json);
					if (jsonArray.length() > 0) {
						JSONObject jsonObj = new JSONObject();
						jsonObj.put("results", jsonArray);

						intent = new Intent(MainActivity.this, SearchResultActivity.class);
						intent.putExtra("favorite", jsonObj.toString());
						startActivity(intent);
					}
					else {
						Toast.makeText(getApplicationContext(), NO_FAVORITES, Toast.LENGTH_LONG).show();
					}
				}
				catch (JSONException e1) {
					e1.printStackTrace();
				}
			}
			else {
				Toast.makeText(getApplicationContext(), DATABASE_ERROR, Toast.LENGTH_LONG).show();
			}
			return true;

		case R.id.action_catogry:
			intent = new Intent(MainActivity.this, CategoryActivity.class);
			startActivity(intent);
			return true;

		case R.id.action_logout:
			session.logoutUser();
			return true;
		}

		return false;
	}

	@Override
	public void onResume() {
		super.onResume();
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		Criteria criteria = new Criteria();
		provider = locationManager.getBestProvider(criteria, false);
		Location location = locationManager.getLastKnownLocation(provider);

		if (location != null) {
			System.out.println("Provider " + provider + " has been selected.");
			onLocationChanged(location);
		}
		else {
			latitude = 0;
			longitude = 0;
		}
		isLoggedIn = session.isLoggedIn();
		this.invalidateOptionsMenu();

		if (isLoggedIn) {
			loginButton.setVisibility(View.GONE);
			mUsernameView.setVisibility(View.VISIBLE);
			mUsernameView.setText(session.getUserDetails().get(USERNAME));
		}
		else {
			loginButton.setVisibility(View.VISIBLE);
			mUsernameView.setVisibility(View.GONE);
		}
	}

	private boolean isAdmin() {
		return ADMIN_EMAIL.equals(session.getUserDetails().get(EMAIL));
	}

	@Override
	protected void onPause() {
		super.onPause();
		locationManager.removeUpdates(this);
	}

	@Override
	public void onLocationChanged(Location location) {
		latitude = location.getLatitude();
		longitude = location.getLongitude();
	}

	@Override
	public void onProviderDisabled(String arg0) {

	}

	@Override
	public void onProviderEnabled(String arg0) {

	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {

	}
}
