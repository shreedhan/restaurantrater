/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.android.Utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

/**
 * @author shreedhan
 * Utilities to handle all HTTP communication between backend and frontend
 */
public class HTTPUtilities {

	public static HttpResponse put(String url, JSONObject putData) {
		return executeRequest(url, putData, "PUT");
	}

	public static HttpResponse post(String url, JSONObject postData) {
		return executeRequest(url, postData, "POST");
	}

	public static HttpResponse delete(String url) {
		return executeRequest(url, null, "DELETE");
	}

	public static HttpResponse get(String url) {
		return executeRequest(url, null, "GET");
	}

	private static HttpResponse executeRequest(String url, JSONObject postData, String action) {

		HttpResponse response = null;

		StringEntity se = null;
		if (postData != null)
			try {
				se = new StringEntity(postData.toString());
				// set request content type
				se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			}
			catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}
		response = execute(url, action, se);
		return response;
	}

	/**
	 * @param url
	 * @param action
	 * @param se
	 * @return
	 */
	private static HttpResponse execute(String url, String action, StringEntity se) {
		HttpClient httpClient = new DefaultHttpClient();
		HttpResponse response = null;
		try {
			if (action.equalsIgnoreCase("get")) {
				HttpGet httpGet = new HttpGet(url);
				response = httpClient.execute(httpGet);
			}
			if (action.equalsIgnoreCase("put")) {
				HttpPut httpPut = new HttpPut(url);
				httpPut.setEntity(se);
				response = httpClient.execute(httpPut);

			}
			else if (action.equalsIgnoreCase("post")) {
				HttpPost httpPost = new HttpPost(url);
				httpPost.setEntity(se);
				response = httpClient.execute(httpPost);

			}
			else if (action.equalsIgnoreCase("delete")) {
				HttpDelete httpDelete = new HttpDelete(url);
				response = httpClient.execute(httpDelete);
			}
		}
		catch (Exception e) {
			System.out.println("Error executing");
			e.printStackTrace();
		}
		return response;
	}

	public static String getString(HttpEntity entity) {
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent()), 65728);
			String line = null;

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
}
