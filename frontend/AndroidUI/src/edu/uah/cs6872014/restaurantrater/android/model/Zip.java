/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.android.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Suman
 *
 */
public class Zip {

	private int		zipcode;
	private String	city;
	private String	state;

	/**
	 * @return the zipcode
	 */
	public int getZipcode() {
		return zipcode;
	}

	/**
	 * @param zipcode
	 *            the zipcode to set
	 */
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	public JSONObject toJSON() {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("zipcode", this.zipcode).put("city", this.city).put("state", this.state);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObj;
	}

	public static Zip toZip(String jsonZip) {
		Zip zip = new Zip();
		try {
			JSONObject jsonObject = new JSONObject(jsonZip);
			zip.zipcode = jsonObject.getInt("zipcode");
			zip.city = jsonObject.getString("city");
			zip.state = jsonObject.getString("state");
		}
		catch (JSONException e) {
			e.printStackTrace();
			System.out.println("Couldn't parse JSON " + zip.zipcode);
			if (zip.zipcode <= 0)
				return null;
		}

		return zip;
	}
}
