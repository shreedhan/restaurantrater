package edu.uah.cs6872014.restaurantrater.android.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Suman
 *
 */
public class Category {

	private int		cid;
	private String	name;
	private String	description;

	/**
	 * @return the cid
	 */
	public int getCid() {
		return cid;
	}

	/**
	 * @param cid
	 *            the cid to set
	 */
	public void setCid(int cid) {
		this.cid = cid;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	public JSONObject toJSON(){
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("cid", this.cid).put("name", this.name).put("description", this.description);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObj;
	}
	
	public static Category toCategory(String jsonCategory){
		Category category = new Category();
		try {
			JSONObject jsonObj = new JSONObject(jsonCategory);
			
			category.cid = jsonObj.getInt("cid");
			category.name = jsonObj.getString("name");
			category.description = jsonObj.getString("description");
		}
		catch (JSONException e) {
			e.printStackTrace();
			
		}
		
		return category;
	}
}
