/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.android.activity.adapter;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;
import edu.uah.cs6872014.restaurantrater.android.R;
import edu.uah.cs6872014.restaurantrater.android.Utilities.Constants;

/**
 * @author shreedhan
 * 
 */
public class SearchResultAdapter extends ArrayAdapter<String> {

	private final Context					context;
	private final String[]					results;
	private final HashMap<String, String>	ratingmap;

	public SearchResultAdapter(Context context, String[] results, HashMap<String, String> ratingmap) {
		super(context, R.layout.list_search_result, results);
		this.context = context;
		this.results = results;
		this.ratingmap = ratingmap;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View vRow = inflater.inflate(R.layout.list_search_result, parent, false);
		TextView tvName = (TextView) vRow.findViewById(R.id.name);
		TextView tvDetail = (TextView) vRow.findViewById(R.id.detail);
		TextView tvGoogleRating = (TextView) vRow.findViewById(R.id.googlerating);
		RatingBar rbRating = (RatingBar) vRow.findViewById(R.id.rating);
		String result = results[position];
		String staddress = null;
		String gid = null;

		try {
			JSONObject resultObj = new JSONObject(result);
			gid = resultObj.getString("id");
			String name = resultObj.getString("name");
			tvName.setText(name);
			if (resultObj.has("formatted_address")) {
				staddress = resultObj.getString("formatted_address");
				tvDetail.setText(staddress);
			}
			if (resultObj.has("vicinity")) {
				staddress = resultObj.getString("vicinity");
				tvDetail.setText(staddress);
			}
			
			if (Math.round(Double.parseDouble(resultObj.getString("rating"))) > 0){
				tvGoogleRating.setText("Google Rating : "
					+ Constants.QUALITY[(int) Math.round(Double.parseDouble(resultObj.getString("rating")))]);
			}
			
			if (ratingmap != null && ratingmap.containsKey(gid))
			{
				rbRating.setRating((float) Double.parseDouble(ratingmap.get(gid)));
				Log.d("madhu", ratingmap.get(gid));
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
		}

		return vRow;
	}
}
