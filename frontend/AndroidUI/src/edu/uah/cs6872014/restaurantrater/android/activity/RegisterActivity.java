package edu.uah.cs6872014.restaurantrater.android.activity;

import java.sql.Timestamp;
import java.util.Date;

import org.apache.http.HttpResponse;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import edu.uah.cs6872014.restaurantrater.android.R;
import edu.uah.cs6872014.restaurantrater.android.Utilities.Constants;
import edu.uah.cs6872014.restaurantrater.android.Utilities.HTTPUtilities;
import edu.uah.cs6872014.restaurantrater.android.Utilities.UserUtilities;
import edu.uah.cs6872014.restaurantrater.android.model.User;

/**
 * Activity which displays a registration screen to the user.
 */

/**
 * @author Suman
 *
 */
public class RegisterActivity extends Activity {
	
	private UserRegisterTask	mAuthTask	= null;

	// Values for username, email and password at the time of the registration attempt.
	private String				mEmail;
	private String				mPassword;
	private String				mUsername;

	// UI references.
	private EditText			mEmailView;
	private EditText			mUsernameView;
	private EditText			mPasswordView;
	private Button				mRegisterButton;

	private View				mRegistrationFormView;
	private View				mRegistrationStatusView;
	private TextView			mRegistrationStatusMessageView;

	private static String		SUCCESS		= "Registration Successful!";
	private static String		FAILURE		= "Registration Failure!";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		mEmailView = (EditText) findViewById(R.id.reg_email);
		mUsernameView = (EditText) findViewById(R.id.reg_username);
		mPasswordView = (EditText) findViewById(R.id.reg_password);
		mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView textView, int id, KeyEvent event) {
				if (id == R.id.register || id == EditorInfo.IME_NULL) {
					attemptRegistration();
					return true;
				}
				return false;
			}
		});

		mRegistrationFormView = findViewById(R.id.register_form);
		mRegistrationStatusView = findViewById(R.id.register_status);
		mRegistrationStatusMessageView = (TextView) findViewById(R.id.register_status_message);

		mRegisterButton = (Button) findViewById(R.id.register_button);
		mRegisterButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				attemptRegistration();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.register, menu);
		return true;
	}

	/**
	 * Attempts to register the account specified by the login form. If there are form errors (invalid email, missing
	 * fields, etc.), the errors are presented and no actual registration attempt is made.
	 */
	public void attemptRegistration() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		mUsername = mUsernameView.getText().toString();
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		if(TextUtils.isEmpty(mUsername)){
			mUsernameView.setError(getString(R.string.error_field_required));
			focusView = mUsernameView;
			cancel = true;
		}
		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		}
		else if (!mEmail.contains("@")) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		}

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		}
		else if (mPassword.length() < 6) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt registration and focus the first
			// form field with an error.
			focusView.requestFocus();
		}
		else {
			// Show a progress spinner, and kick off a background task to
			// perform the user registration attempt.
			mRegistrationStatusMessageView.setText(R.string.register_progress);
			showProgress(true);
			mAuthTask = new UserRegisterTask();
			mAuthTask.execute((Void) null);
		}
	}

	/**
	 * Shows the progress UI and hides the registration form.
	 * Provided by the ADT
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

			mRegistrationStatusView.setVisibility(View.VISIBLE);
			mRegistrationStatusView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0)
				.setListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						mRegistrationStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
					}
				});

			mRegistrationFormView.setVisibility(View.VISIBLE);
			mRegistrationFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1)
				.setListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						mRegistrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
					}
				});
		}
		else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mRegistrationStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mRegistrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous registration task used to register the user.
	 */
	public class UserRegisterTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {

			User user = new User();
			user.setUid(-1);
			user.setUsername(mUsername);
			user.setEmail(mEmail);
			user.setPassword(UserUtilities.sha256Encrypt(mPassword));
			user.setFname("");
			user.setMidname("");
			user.setLname("");
			user.setStaddress("");
			user.setZipcode(00000);
			Timestamp ts = new Timestamp(new Date().getTime());
			user.setCreatedts(ts);
			user.setLastlogints(ts);

			String url = Constants.USER_URL;
			HttpResponse response = HTTPUtilities.post(url, user.toJSON());
			int statuscode = response.getStatusLine().getStatusCode();
			if (statuscode == 200) {
				return true;
			}
			return false;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;

			if (success) {
				finish();
				showProgress(false);
				Toast.makeText(getApplicationContext(), SUCCESS, Toast.LENGTH_SHORT).show();
			}
			else {
				showProgress(false);
				mEmailView.setError(getString(R.string.error_email_exists));
				mEmailView.requestFocus();
				Toast.makeText(getApplicationContext(), FAILURE, Toast.LENGTH_LONG).show();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}

}
