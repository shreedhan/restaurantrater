/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.android.Utilities;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * @author shreedhan
 * All rest server communcations functions. These functions uses HTTPutilities functions for communication with backend 
 * 
 */
public class RESTUtilities {
	public static String getDataFromURL(String url) {
		HttpResponse response = HTTPUtilities.get(url);
		if (response == null || response.getStatusLine().getStatusCode() != 200)
			return null;
		String jsonResponse = HTTPUtilities.getString(response.getEntity());
		return jsonResponse;
	}

	public static String getRestaurant(String gid) {
		String url = Constants.RESTAURANT_URL + "gid/" + gid;
		return getDataFromURL(url);
	}

	public static String getCategoriesCount() {
		String url = Constants.CATEGORY_COUNT_URL;
		return getDataFromURL(url);
	}

	public static String getAvgRatingByCategory(String gid, int cid) {
		String url = Constants.RATE_AGG_URL + "gid/&" + gid + "&" + cid;
		return getDataFromURL(url);
	}

	public static String getOverallAvgRating(ArrayList<String> gidArray) {
		String url = Constants.RATE_AGG_URL + "group/null";
		for (int i = 0; i < gidArray.size(); i++) {
			url = url + ",'" + gidArray.get(i) + "'";
		}
		return getDataFromURL(url);
	}

	public static String getAllCategories() {
		String url = Constants.CATEGORY_URL;
		return getDataFromURL(url);

	}

	public static String fetchComments(int rid) {
		String url = Constants.COMMENT_URL + rid;
		return getDataFromURL(url);
	}

	public static String fetchCommentsAndRatingsOfRestaurant(int rid) {
		String url = Constants.RATE_AGG_URL + "rid/" + rid;
		return getDataFromURL(url);
	}

	public static String fetchUserRestaurantComments(int uid, int rid) {
		String url = Constants.COMMENT_URL + uid + "&" + rid;
		return getDataFromURL(url);
	}

	public static int fetchUserRestaurantCategoryRating(int uid, int rid, int cid) {
		String url = Constants.RATE_URL + uid + "&" + rid + "&" + cid;
		String fetchRate = getDataFromURL(url);
		if (fetchRate != null) {
			try {
				JSONObject jsonRate = new JSONObject(fetchRate);
				return jsonRate.getInt("star");
			}
			catch (JSONException e) {
				return 0;
			}
		}
		else {
			return 0;
		}
	}

	public static HttpResponse writeRestaurant(String gid, String name, String stAddress, int zipcode, double latitude,
		double longitude) {
		JSONObject restaurantJsonData = toRestaurantJSON(gid, name, stAddress, zipcode, latitude, longitude);
		return HTTPUtilities.post(Constants.RESTAURANT_URL, restaurantJsonData);
		
	}

	public static HttpResponse writeComment(int uid, int rid, String commentText) {
		JSONObject commentJsonData = toCommentJSON(uid, rid, commentText);
		return HTTPUtilities.post(Constants.COMMENT_URL, commentJsonData);
	}

	public static HttpResponse deleteComment(int uid, int rid) {
		String url = Constants.COMMENT_URL + uid + "&" + rid;
		Log.d("madhu", url);
		return HTTPUtilities.delete(url);
	}

	public static HttpResponse deleteRating(int uid, int rid, int cid) {
		String url = Constants.RATE_URL + uid + "&" + rid + "&" + cid;
		Log.d("madhu", url);
		return HTTPUtilities.delete(url);
	}

	public static HttpResponse writeRating(int uid, int rid, int cid, int star) {
		JSONObject commentJsonData = toRateJSON(uid, rid, cid, star);
		return HTTPUtilities.post(Constants.RATE_URL, commentJsonData);
	}

	public static JSONObject toCommentJSON(int uid, int rid, String commentText) {
		JSONObject jsonObj = new JSONObject();
		Timestamp ts = new Timestamp(new Date().getTime());
		try {
			jsonObj.put("uid", uid).put("rid", rid).put("comment", commentText).put("createdts", ts)
				.put("modifiedts", ts);
		}
		catch (JSONException e) {
			Log.d("madhu", "Failed to create json");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObj;
	}

	public static JSONObject toRateJSON(int uid, int rid, int cid, int star) {
		JSONObject jsonObj = new JSONObject();
		Timestamp ts = new Timestamp(new Date().getTime());
		try {
			jsonObj.put("uid", uid).put("rid", rid).put("cid", cid).put("star", star).put("createdts", ts)
				.put("modifiedts", ts);
		}
		catch (JSONException e) {
			Log.d("madhu", "Failed to create json");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObj;
	}

	public static JSONObject toRestaurantJSON(String gid, String name, String stAddress, int zipcode, double latitude,
		double longitude) {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("gid", gid).put("staddress", stAddress).put("name", name).put("zipcode", zipcode)
				.put("latitude", latitude).put("longitude", longitude);
		}
		catch (JSONException e) {
			Log.d("madhu", "Failed to create json");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObj;
	}

	public static String toggleFavorite(int uid, JSONObject restaurant, String action) {
		JSONObject favorite = new JSONObject();
		Timestamp ts = new Timestamp(new Date().getTime());
		HttpResponse response = null;
		String result = null;
		try {
			favorite.put("uid", uid).put("createdts", ts);
			favorite.put("gid", restaurant.getString("gid"));
			favorite.put("restName", restaurant.get("restName"));
			favorite.put("restAddress", restaurant.get("restAddress"));
			favorite.put("restZip", restaurant.get("restZip"));
			favorite.put("lat", restaurant.get("lat"));
			favorite.put("lng", restaurant.get("lng"));
			response = HTTPUtilities.post(Constants.FAVORITE_URL, favorite);
			String stringResponse = null;
			if (response != null)
				stringResponse = HTTPUtilities.getString(response.getEntity());
			if (response == null || response.getStatusLine().getStatusCode() != 200) {
				return null;
			}
			else {
				JSONObject jsonResponse = new JSONObject(stringResponse);
				result = null;
				if (jsonResponse.has("status")) {
					result = jsonResponse.getString("status");
				}
				return result;
			}

		}
		catch (JSONException e) {
			Log.d("shree", "Failed to create json");
			e.printStackTrace();
			return null;
		}
	}
}
