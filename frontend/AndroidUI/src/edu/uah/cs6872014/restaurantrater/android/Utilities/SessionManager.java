package edu.uah.cs6872014.restaurantrater.android.Utilities;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import edu.uah.cs6872014.restaurantrater.android.activity.LoginActivity;
import edu.uah.cs6872014.restaurantrater.android.activity.MainActivity;
/**
 * Class for handling session data for logged user.
 */

/**
 * @author Suman
 * 
 */
public class SessionManager {

	SharedPreferences			pref			= null;
	Editor						editor			= null;
	Context						context;

	int							PRIVATE_MODE	= 0;
	private static final String	PREF_NAME		= "RestaurantRaterPref";

	private static final String	IS_LOGIN		= "IsLoggedIn";
	private static final String	KEY_UID			= "uid";
	private static final String	KEY_USERNAME	= "username";
	private static final String	KEY_EMAIL		= "email";

	@SuppressLint("CommitPrefEdits")
	public SessionManager(Context context) {
		this.context = context;
		pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	public void createLoginSession(String uid, String username, String email) {
		editor.putBoolean(IS_LOGIN, true);
		editor.putString(KEY_UID, uid);
		editor.putString(KEY_EMAIL, email);
		editor.putString(KEY_USERNAME, username);

		editor.commit();
	}

	public void checkLogin() {
		// Check login status
		if (!this.isLoggedIn()) {
			// User is not logged in redirect him to Login Activity
			Intent intent = new Intent(context, LoginActivity.class);

			// Closing all the Activities
			// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			// Add new Flag to start new Activity
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			// Starting Login Activity
			context.startActivity(intent);
		}
	}

	public HashMap<String, String> getUserDetails() {
		HashMap<String, String> user = new HashMap<String, String>();
		user.put(KEY_UID, pref.getString(KEY_UID, null));
		user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));
		user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));

		return user;
	}

	public void logoutUser() {
		editor.clear();
		editor.commit();

		Intent intent = new Intent(context, MainActivity.class);
		// Closing all the Activities
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra("EXIT", true);

		// Add new Flag to start new Activity
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		// Starting Login Activity
		context.startActivity(intent);

	}

	public boolean isLoggedIn() {
		return pref.getBoolean(IS_LOGIN, false);
	}
}
