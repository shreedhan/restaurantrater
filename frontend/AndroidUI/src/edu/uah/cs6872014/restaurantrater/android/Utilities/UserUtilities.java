package edu.uah.cs6872014.restaurantrater.android.Utilities;

import java.security.MessageDigest;

import org.apache.http.HttpResponse;

import edu.uah.cs6872014.restaurantrater.android.model.Category;
import edu.uah.cs6872014.restaurantrater.android.model.User;

/**
 * Utility class that provides HTTP services for accessing different information related to logged user.
 */

/**
 * @author Suman
 * 
 */
public class UserUtilities {

	public static String sha256Encrypt(String base) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(base.getBytes("UTF-8"));
			StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}
			return hexString.toString();
		}
		catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public static HttpResponse getUserData(String email) {
		String url = Constants.USER_URL + "email/" + email;
		return HTTPUtilities.get(url);
	}

	public static HttpResponse updateUser(User user) {
		String url = Constants.USER_URL + user.getUid();
		return HTTPUtilities.put(url, user.toJSON());
	}

	public static HttpResponse getFavoriteRestaurants(String uid) {
		String url = Constants.FAVORITE_URL + "uid/" + uid;
		return HTTPUtilities.get(url);
	}

	public static HttpResponse addCategory(Category category){
		String url = Constants.CATEGORY_URL;
		return HTTPUtilities.post(url, category.toJSON());
	}
}
