package edu.uah.cs6872014.restaurantrater.android.model;

import java.sql.Timestamp;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Suman
 *
 */
public class Rate {

	private int			uid;
	private int			rid;
	private int			cid;
	private int			star;
	private Timestamp	createdts;
	private Timestamp	modifiedts;

	/**
	 * @return the uid
	 */
	public int getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(int uid) {
		this.uid = uid;
	}

	/**
	 * @return the rid
	 */
	public int getRid() {
		return rid;
	}

	/**
	 * @param rid
	 *            the rid to set
	 */
	public void setRid(int rid) {
		this.rid = rid;
	}

	/**
	 * @return the cid
	 */
	public int getCid() {
		return cid;
	}

	/**
	 * @param cid
	 *            the cid to set
	 */
	public void setCid(int cid) {
		this.cid = cid;
	}

	/**
	 * @return the star
	 */
	public int getstar() {
		return star;
	}

	/**
	 * @param star
	 *            the star to set
	 */
	public void setstar(int star) {
		this.star = star;
	}

	/**
	 * @return the createdts
	 */
	public Timestamp getCreatedts() {
		return createdts;
	}

	/**
	 * @param createdts
	 *            the createdts to set
	 */
	public void setCreatedts(Timestamp createdts) {
		this.createdts = createdts;
	}

	/**
	 * @return the modifiedts
	 */
	public Timestamp getModifiedts() {
		return modifiedts;
	}

	/**
	 * @param modifiedts
	 *            the modifiedts to set
	 */
	public void setModifiedts(Timestamp modifiedts) {
		this.modifiedts = modifiedts;
	}

	public JSONObject toJSON() {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("uid", this.uid).put("rid", this.rid).put("cid", this.cid).put("star", this.star)
				.put("createdts", this.createdts).put("modifiedts", this.modifiedts);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObj;
	}

	public static Rate toRate(String jsonRate) {
		Rate rate = new Rate();
		try {
			JSONObject jsonObj = new JSONObject(jsonRate);
			rate.uid = jsonObj.getInt("uid");
			rate.rid = jsonObj.getInt("rid");
			rate.cid = jsonObj.getInt("cid");
			rate.star = jsonObj.getInt("star");
			rate.createdts = Timestamp.valueOf(jsonObj.getString("createdts"));
			rate.modifiedts = Timestamp.valueOf(jsonObj.getString("modifiedts"));
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Couldn't parse JSON for uid: " + rate.uid + ", rid : " + rate.rid + " & cid : "
				+ rate.cid);
			if (rate.uid <= 0 || rate.rid <= 0 || rate.cid <= 0)
				return null;
		}
		return rate;
	}
}
