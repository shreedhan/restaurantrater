/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.android.Utilities;

/**
 * @author shreedhan
 * 
 */
public class Constants {
	// Developer API KEY for Map/Places
	public final static String		GOOGLE_API_KEY			= "AIzaSyAKDMhLzQpK-EgmATi-LiSy7mscfTk9xuU";
	// public final static String GOOGLE_API_KEY = "AIzaSyCQ3tk9VWZjTsy7T7qP4ByduyXK8lgJJC8";
	public final static String		GOOGLE_PHOTO_URL		= "https://maps.googleapis.com/maps/api/place/photo?maxwidth=1024&sensor=true&key="
																+ GOOGLE_API_KEY + "&photoreference=";
	public final static String		GOOGLE_PLACE_URL		= "https://maps.googleapis.com/maps/api/place/textsearch/json?sensor=false&key="
																+ GOOGLE_API_KEY + "&query=restaurants+in+";
	public final static String		GOOGLE_LOCATION_URL		= "https://maps.googleapis.com/maps/api/place/nearbysearch/json?radius=5000&types=restaurant&sensor=false&key="
																+ GOOGLE_API_KEY + "&location=";
	public final static String		GOOGLE_PLACE_DETAIL_URL	= "https://maps.googleapis.com/maps/api/place/details/json?sensor=true&key="
																+ GOOGLE_API_KEY + "&reference=";

	public final static String		GOOGLE_SINGLE_PLACE_URL	= "https://maps.googleapis.com/maps/api/place/nearbysearch/json?radius=1&sensor=false&key="
																+ GOOGLE_API_KEY + "&location=";

	// IP Address of the Rest Server
	public final static String		IP_ADDRESS				= "http://192.168.1.108:8080";

	// REST url of the rest server
	public final static String		REST_URL				= IP_ADDRESS + "/restaurant-rater/services";

	// Service specific url
	public final static String		USER_URL				= REST_URL + "/users/";
	public final static String		RESTAURANT_URL			= REST_URL + "/restaurants/";
	public final static String		CATEGORY_URL			= REST_URL + "/categories/";
	public static final String		COMMENT_URL				= REST_URL + "/comments/";

	public static final String		CATEGORY_COUNT_URL		= CATEGORY_URL + "count";
	public final static String		RATE_AGG_URL			= REST_URL + "/rates/agg/";
	public final static String		RATE_URL				= REST_URL + "/rates/";
	public static final String		FAVORITE_URL			= REST_URL + "/favorites/";

	public final static String[]	QUALITY					= { "Not Available", "Bad", "Poor", "Fair", "Good", "Excellent" };

}
