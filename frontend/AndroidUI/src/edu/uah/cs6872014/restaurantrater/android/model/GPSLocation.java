/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.android.model;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

/**
 * @author Madhu Sigdel
 * 
 */
public class GPSLocation implements LocationListener {

	private static double	latitude;
	private static double	longitude;

	public static double getLatitude() {
		return latitude;
	}

	public static void setLatitude(double latitude) {
		GPSLocation.latitude = latitude;
	}

	public static double getLongitude() {
		return longitude;
	}

	public static void setLongitude(double longitude) {
		GPSLocation.longitude = longitude;
	}

	@Override
	public void onLocationChanged(Location loc) {
		loc.getLatitude();
		loc.getLongitude();
		latitude = loc.getLatitude();
		longitude = loc.getLongitude();
	}

	@Override
	public void onProviderDisabled(String provider) {
		// print "Currently GPS is Disabled";
	}

	@Override
	public void onProviderEnabled(String provider) {
		// print "GPS got Enabled";
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

}
