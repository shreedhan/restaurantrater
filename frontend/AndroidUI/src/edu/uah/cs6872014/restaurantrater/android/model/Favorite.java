package edu.uah.cs6872014.restaurantrater.android.model;

import java.sql.Timestamp;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Suman
 *
 */
public class Favorite {

	private int			uid;
	private int			rid;
	private Timestamp	createdts;

	/**
	 * @return the uid
	 */
	public int getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(int uid) {
		this.uid = uid;
	}

	/**
	 * @return the rid
	 */
	public int getRid() {
		return rid;
	}

	/**
	 * @param rid
	 *            the rid to set
	 */
	public void setRid(int rid) {
		this.rid = rid;
	}

	/**
	 * @return the createdts
	 */
	public Timestamp getCreatedts() {
		return createdts;
	}

	/**
	 * @param createdts
	 *            the createdts to set
	 */
	public void setCreatedts(Timestamp createdts) {
		this.createdts = createdts;
	}

	public JSONObject toJSON() {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("uid", uid).put("rid", rid).put("createdts", createdts);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObj;
	}

	public static Favorite toFavorite(String jsonFavorite) {
		Favorite favorite = new Favorite();
		try {
			JSONObject jsonObj = new JSONObject(jsonFavorite);
			favorite.uid = jsonObj.getInt("uid");
			favorite.rid = jsonObj.getInt("rid");
			favorite.createdts = Timestamp.valueOf(jsonObj.getString("createdts"));
		}
		catch (JSONException e) {
			System.out.println("Couldn't parse JSON for uid : " + favorite.uid + " & rid : " + favorite.rid);
			if (favorite.uid <= 0 || favorite.rid <= 0)
				return null;
		}
		return favorite;
	}
}
