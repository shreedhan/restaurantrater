package edu.uah.cs6872014.restaurantrater.android.activity;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import edu.uah.cs6872014.restaurantrater.android.R;
import edu.uah.cs6872014.restaurantrater.android.Utilities.Constants;
import edu.uah.cs6872014.restaurantrater.android.Utilities.GoogleUtilities;
import edu.uah.cs6872014.restaurantrater.android.Utilities.RESTUtilities;
import edu.uah.cs6872014.restaurantrater.android.Utilities.SessionManager;
import edu.uah.cs6872014.restaurantrater.android.activity.adapter.CommentAdapter;
import edu.uah.cs6872014.restaurantrater.android.model.Category;
import edu.uah.cs6872014.restaurantrater.android.model.Restaurant;

/**
 * @author shreedhan
 * Activity which displays a restaurant detail. Also handles inserting favorite data, google map and android call integration
 * 
 */
public class RestaurantDetailActivity extends Activity {
	private HashMap<Integer, Category>	categories;
	private SessionManager				session;

	@Override
	protected void onResume() {
		super.onResume();
		session = new SessionManager(getApplicationContext());
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_restaurant_detail);
		Intent intent = getIntent();
		String stringJson = intent.getStringExtra("restObj");
		JSONObject restaurantObj = null;
		try {
			if (stringJson != null)
				restaurantObj = new JSONObject(stringJson);
		}
		catch (JSONException e) {
			e.printStackTrace();
			return;
		}
		if (restaurantObj != null) {
			try {

				final double lat = restaurantObj.getDouble("lat");
				final double lng = restaurantObj.getDouble("lng");
				final String gid = restaurantObj.getString("gid");

				String jsonSingleResponse = GoogleUtilities.getSinglePlaceJSONString(lat, lng);
				JSONObject singleResponseObj = new JSONObject(jsonSingleResponse);

				if (!singleResponseObj.has("results"))
					return;
				JSONArray resultArray = singleResponseObj.getJSONArray("results");
				for (int i = 0; i < resultArray.length(); i++) {
					JSONObject obj = resultArray.getJSONObject(i);
					if (obj.has("id") && obj.getString("id").equals(gid)) {
						restaurantObj = obj;
						break;
					}
				}

				String reference = null;
				reference = restaurantObj.getString("reference");

				final Restaurant restaurant = fetchRestaurantDetailsFromDB(gid);
				String gRestaurant = fetchRestaurantDetailsFromGoogle(reference);
				fetchAllCategories();
				int countCategory = getCountCategoryFromDB();

				JSONObject gRestaurantObj = new JSONObject(gRestaurant);
				gRestaurantObj = gRestaurantObj.getJSONObject("result");
				final String name = gRestaurantObj.getString("name");
				final String stAddress = gRestaurantObj.getString("formatted_address");
				final String phone = gRestaurantObj.getString("formatted_phone_number");
				String gRating = gRestaurantObj.getString("rating");
				getActionBar().setTitle(name);

				TextView tvName = (TextView) findViewById(R.id.name);
				TextView tvAddress = (TextView) findViewById(R.id.address);
				TextView tvPhone = (TextView) findViewById(R.id.phone);
				TextView tvGRating = (TextView) findViewById(R.id.g_rating);
				ImageView ivRestImage = (ImageView) findViewById(R.id.rest_image);
				String photo_reference = null;
				if (restaurantObj.has("photos")) {
					JSONArray photoArray = restaurantObj.getJSONArray("photos");
					JSONObject photoObj = photoArray.getJSONObject(0);
					if (photoObj.has("photo_reference")) {
						photo_reference = photoObj.getString("photo_reference");
					}
					if (photo_reference != null) {
						String url = Constants.GOOGLE_PHOTO_URL + photo_reference;
						Drawable drawable = LoadImageFromWebOperations(url, "");
						ivRestImage.setImageDrawable(drawable);
					}
				}
				for (int count = 1; count <= countCategory; count++) {
					float rating = fetchRatingFromDB(gid, count);
					if (rating <= 0)
						continue;
					RatingBar ratingBar = new RatingBar(this, null, android.R.attr.ratingBarStyleSmall);
					ratingBar.setIsIndicator(true);
					ratingBar.setRating(rating);

					TextView tvCategory = new TextView(this);
					Category category = categories.get(count);
					tvCategory.setText(category.getName());

					View vGrating = findViewById(R.id.rating_layout);
					((ViewGroup) vGrating).addView(tvCategory, new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT));

					((ViewGroup) vGrating).addView(ratingBar, new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT));

				}

				JSONArray commentsArray = null;
				String[] comments = null;
				if (restaurant != null) {
					String jsonComments = RESTUtilities.fetchCommentsAndRatingsOfRestaurant(restaurant.getRid());
					if (jsonComments != null) {
						commentsArray = new JSONArray(jsonComments);

						comments = new String[commentsArray.length()];
						for (int i = 0; i < commentsArray.length(); i++) {
							comments[i] = commentsArray.getString(i);
						}
					}

				}
				if (commentsArray == null || commentsArray.length() == 0) {
					comments = new String[1];
					comments[0] = "{\"fullname\":\"No comments yet\"}";
				}

				CommentAdapter adapter = new CommentAdapter(this, R.layout.list_restaurant_comment,
					R.id.restaurant_comment, comments);
				ListView lvComment = (ListView) findViewById(R.id.comments);
				lvComment.setAdapter(adapter);

				tvName.setText(name);
				tvAddress.setText("Address: " + stAddress);
				tvPhone.setText("Phone: " + phone);
				tvGRating.setText("Google Rating: " + Constants.QUALITY[(int) Math.round(Double.parseDouble(gRating))]);

				ImageButton callPhone = (ImageButton) findViewById(R.id.call_phone);
				ImageButton visitURL = (ImageButton) findViewById(R.id.visit_url);
				ImageButton addFavorite = (ImageButton) findViewById(R.id.favorite);
				ImageButton rateRestaurant = (ImageButton) findViewById(R.id.rate_restaurant);

				callPhone.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (phone != null && !phone.isEmpty()) {
							String uri = "tel:" + phone.trim();
							Intent dialIntent = new Intent(Intent.ACTION_DIAL);
							dialIntent.setData(Uri.parse(uri));
							startActivity(dialIntent);
						}

					}
				});

				final double latitude = lat;
				final double longitude = lng;
				visitURL.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						String label = name;
						String uriBegin = "geo:" + latitude + "," + longitude;
						String query = latitude + "," + longitude + "(" + label + ")";
						String encodedQuery = Uri.encode(query);
						String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
						Uri uri = Uri.parse(uriString);
						Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
						startActivity(intent);

					}
				});

				addFavorite.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						String response = null;

						if (session.isLoggedIn()) {
							int uid = getLoggedInUserID();
							JSONObject restaurant = new JSONObject();
							try {
								restaurant.put("restName", name);
								restaurant.put("restAddress", stAddress);
								restaurant.put("restZip", "00000");
								restaurant.put("gid", gid);
								restaurant.put("lat", lat);
								restaurant.put("lng", lng);
								response = RESTUtilities.toggleFavorite(uid, restaurant, "add");
							}
							catch (JSONException e) {
								e.printStackTrace();
							}
						}
						else {
							Toast toast = Toast.makeText(getApplicationContext(), "Please login to add to favorites",
								Toast.LENGTH_LONG);
							toast.show();
							session.checkLogin();
							return;
						}
						if (response != null) {
							Toast toast = Toast.makeText(getApplicationContext(), response + " \"" + name + "\" "
								+ (response.equalsIgnoreCase("added") ? "to" : "from") + " your favorites",
								Toast.LENGTH_LONG);
							toast.show();
						}
						else {
							Toast toast = Toast.makeText(getApplicationContext(),
								"Could not add to your favorites. Try again later.", Toast.LENGTH_LONG);
							toast.show();
						}
					}
				});

				rateRestaurant.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (session.isLoggedIn()) {
							Intent rateIntent = new Intent(RestaurantDetailActivity.this,
								RestaurantReviewActivity.class);
							rateIntent.putExtra("gid", gid);
							if (restaurant != null) {
								rateIntent.putExtra("rid", restaurant.getRid());
							}
							rateIntent.putExtra("staddress", stAddress);
							rateIntent.putExtra("name", name);
							rateIntent.putExtra("zipcode", 0);
							rateIntent.putExtra("latitude", lat);
							rateIntent.putExtra("longitude", lng);
							startActivity(rateIntent);
							finish();
						}
						else {
							Toast toast = Toast.makeText(getApplicationContext(),
								"Please login to rate this restaurant", Toast.LENGTH_LONG);
							toast.show();
							session.checkLogin();
							return;
						}
					}
				});

			}
			catch (JSONException e) {
				e.printStackTrace();
			}
		}

		// }
	}

	private int getLoggedInUserID() {
		session = new SessionManager(getApplicationContext());
		HashMap<String, String> user = session.getUserDetails();
		int uid = 0;
		if (user.get("uid") != null) {
			uid = Integer.parseInt(user.get("uid"));

		}
		return uid;
	}

	@SuppressLint("UseSparseArrays")
	private void fetchAllCategories() {
		String stringCategories = RESTUtilities.getAllCategories();
		if (stringCategories == null)
			return;
		try {
			JSONArray jsonCategories = new JSONArray(stringCategories);
			categories = new HashMap<Integer, Category>();
			for (int i = 0; i < jsonCategories.length(); i++) {
				String jsonCategory = jsonCategories.getString(i);
				Category category = Category.toCategory(jsonCategory);
				categories.put(category.getCid(), category);
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private float fetchRatingFromDB(String id, int count) {
		float avgStar = 0;
		String stringRating = RESTUtilities.getAvgRatingByCategory(id, count);
		if (stringRating == null)
			return 0;
		JSONObject jsonRating = null;
		try {
			jsonRating = new JSONObject(stringRating);
			avgStar = (float) jsonRating.getDouble("avgstar");
		}
		catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
		return avgStar;
	}

	private int getCountCategoryFromDB() {
		int count = 0;
		String stringCount = RESTUtilities.getCategoriesCount();
		if (stringCount == null)
			return 0;
		JSONObject jsonCount = null;
		try {
			jsonCount = new JSONObject(stringCount);
			count = jsonCount.getInt("count");
		}
		catch (JSONException e) {
			e.printStackTrace();
			return 0;
		}
		return count;
	}

	private Restaurant fetchRestaurantDetailsFromDB(String id) {
		if (id == null)
			return null;
		String jsonRestaurant = RESTUtilities.getRestaurant(id);
		Restaurant restaurant = null;
		if (jsonRestaurant != null)
			restaurant = Restaurant.toRestaurant(jsonRestaurant);
		return restaurant;
	}

	private String fetchRestaurantDetailsFromGoogle(String reference) {
		String restaurantDetail = null;
		if (reference != null) {
			restaurantDetail = GoogleUtilities.getPlaceDetailJSONString(reference);
			// try {
			// JSONObject jsonGRestaurant = new JSONObject(restaurantDetail);
			// }
			// catch (JSONException e) {
			// e.printStackTrace();
			// }

		}
		return restaurantDetail;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.restaurant_detail, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public static Drawable LoadImageFromWebOperations(String url, String src) {
		try {
			InputStream is = (InputStream) new URL(url).getContent();
			Drawable d = Drawable.createFromStream(is, src);
			return d;
		}
		catch (Exception e) {
			return null;
		}
	}

}
