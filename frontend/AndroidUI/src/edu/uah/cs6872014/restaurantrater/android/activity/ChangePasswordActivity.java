package edu.uah.cs6872014.restaurantrater.android.activity;

import org.apache.http.HttpResponse;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import edu.uah.cs6872014.restaurantrater.android.R;
import edu.uah.cs6872014.restaurantrater.android.Utilities.HTTPUtilities;
import edu.uah.cs6872014.restaurantrater.android.Utilities.SessionManager;
import edu.uah.cs6872014.restaurantrater.android.Utilities.UserUtilities;
import edu.uah.cs6872014.restaurantrater.android.model.User;

/**
 * Activity which displays a screen to allow user to change password.
 */

/**
 * @author Suman
 *
 */
public class ChangePasswordActivity extends Activity {

	// Values for old password and new passwords at the time of the password change attempt.
	private String			mOldPassword;
	private String			mNewPassword1;
	private String			mNewPassword2;

	// UI references.
	private EditText		mOldPasswordView;
	private EditText		mNewPassword1View;
	private EditText		mNewPassword2View;
	private Button			mCancelButton;
	private Button			mSubmitButton;

	SessionManager			session	= null;
	private User			user	= null;

	private static String	EMAIL	= "email";
	private static String	SUCCESS	= "Password changed successfully!";
	private static String	FAILURE	= "Unsuccessfull";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_password);

		session = new SessionManager(getApplicationContext());

		HttpResponse response = UserUtilities.getUserData(session.getUserDetails().get(EMAIL));
		int statuscode = response.getStatusLine().getStatusCode();
		if (statuscode == 200) {
			String userString = HTTPUtilities.getString(response.getEntity());
			user = User.toUser(userString);
		}

		mOldPasswordView = (EditText) findViewById(R.id.old_password);
		mNewPassword1View = (EditText) findViewById(R.id.new_password1);
		mNewPassword2View = (EditText) findViewById(R.id.new_password2);

		mNewPassword2View.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView textView, int id, KeyEvent event) {
				if (id == R.id.change || id == EditorInfo.IME_NULL) {
					changePassword();
					return true;
				}
				return false;
			}
		});

		mSubmitButton = (Button) findViewById(R.id.submit_button);
		mSubmitButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				changePassword();
			}
		});

		mCancelButton = (Button) findViewById(R.id.cancel_button);
		mCancelButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
				return;
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.change_password, menu);
		return true;
	}

	public void changePassword() {

		// Reset errors.
		mOldPasswordView.setError(null);
		mNewPassword1View.setError(null);
		mNewPassword2View.setError(null);

		mOldPassword = mOldPasswordView.getText().toString();
		mNewPassword1 = mNewPassword1View.getText().toString();
		mNewPassword2 = mNewPassword2View.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mOldPassword)) {
			mOldPasswordView.setError(getString(R.string.error_field_required));
			focusView = mOldPasswordView;
			cancel = true;
		}
		else if (!UserUtilities.sha256Encrypt(mOldPassword).equals(user.getPassword())) {
			mOldPasswordView.setError(getString(R.string.error_password_incorrect));
			focusView = mOldPasswordView;
			cancel = true;
		}
		else if (TextUtils.isEmpty(mNewPassword1)) {
			mNewPassword1View.setError(getString(R.string.error_field_required));
			focusView = mNewPassword1View;
			cancel = true;
		}
		else if (mNewPassword1.length() < 6) {
			mNewPassword1View.setError(getString(R.string.error_invalid_password));
			focusView = mNewPassword1View;
			cancel = true;
		}

		else if (!mNewPassword2.equals(mNewPassword1)) {
			mNewPassword2View.setError(getString(R.string.error_password_not_match));
			focusView = mNewPassword2View;
			cancel = true;
		}

		if (cancel) {
			focusView.requestFocus();
		}
		else {
			change();
		}
	}

	private void change() {
		User mUser = new User();

		mUser.setUid(user.getUid());
		mUser.setUsername(user.getUsername());
		mUser.setEmail(user.getEmail());
		mUser.setFname(user.getFname());
		mUser.setMidname(user.getMidname());
		mUser.setLname(user.getLname());
		mUser.setStaddress(user.getStaddress());
		mUser.setZipcode(user.getZipcode());
		mUser.setLastlogints(user.getLastlogints());
		mUser.setCreatedts(user.getCreatedts());

		mUser.setPassword(UserUtilities.sha256Encrypt(mNewPassword1));
		HttpResponse response = UserUtilities.updateUser(mUser);
		int statusCode = response.getStatusLine().getStatusCode();
		if (statusCode == 200) {
			user = mUser;
			Toast.makeText(getApplicationContext(), SUCCESS, Toast.LENGTH_LONG).show();
			finish();
		}
		else {
			Toast.makeText(getApplicationContext(), FAILURE, Toast.LENGTH_LONG).show();
		}
	}
}
