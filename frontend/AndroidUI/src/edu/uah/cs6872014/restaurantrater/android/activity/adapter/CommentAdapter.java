/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.android.activity.adapter;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;
import edu.uah.cs6872014.restaurantrater.android.R;

/**
 * @author shreedhan
 * 
 */
public class CommentAdapter extends ArrayAdapter<String> {

	private final Context	context;
	private final String[]	comments;

	public CommentAdapter(Context context, int listRestaurantComment, int comment, String[] comments) {
		super(context, listRestaurantComment, comment, comments);
		this.context = context;
		this.comments = comments;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		super.getView(position, convertView, parent);
		if (comments == null) {
			TextView tvNone = new TextView(getContext());
			tvNone.setText("No comments yet");
			return tvNone;
		}
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View vRow = inflater.inflate(R.layout.list_restaurant_comment, parent, false);
		String name = null;
		Timestamp date = null;
		String comment = null;
		JSONArray ratingArray = null;
		try {
			JSONObject commentObj = new JSONObject(comments[position]);
			if (commentObj.has("fullname"))
				name = commentObj.getString("fullname");
			if (commentObj.has("rating")) {
				Object ratingObj = commentObj.get("rating");
				if (ratingObj != null && (ratingObj instanceof JSONArray))
					ratingArray = commentObj.getJSONArray("rating");
			}
			if (commentObj.has("modifiedts"))
				date = Timestamp.valueOf(commentObj.getString("modifiedts"));
			if (commentObj.has("comment"))
				comment = commentObj.getString("comment");
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		TextView tvName = (TextView) vRow.findViewById(R.id.username);
		TextView tvDate = (TextView) vRow.findViewById(R.id.comment_date);
		TextView tvComment = (TextView) vRow.findViewById(R.id.restaurant_comment);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm");
		tvName.setText(name);
		if (date != null)
			tvDate.setText(sdf.format(date));
		tvComment.setText(comment);
		if (ratingArray != null) {
			View vGrating = vRow.findViewById(R.id.comment_rating);
			for (int i = 0; i < ratingArray.length(); i++) {
				try {
					JSONObject rating = ratingArray.getJSONObject(i);
					RatingBar ratingBar = new RatingBar(getContext(), null, android.R.attr.ratingBarStyleSmall);
					ratingBar.setIsIndicator(true);
					ratingBar.setRating(rating.getInt("star"));
					TextView tvCategory = new TextView(this.getContext());
					tvCategory.setText(rating.getString("category"));
					LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
					((ViewGroup) vGrating).addView(tvCategory, layoutParams);
					((ViewGroup) vGrating).addView(ratingBar, layoutParams);
				}
				catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return vRow;
	}
}
