package edu.uah.cs6872014.restaurantrater.android.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Suman
 *
 */
public class Restaurant {

	private int		rid;
	private String	gid;
	private String	name;
	private String	staddress;
	private int		zipcode;
	private double longitude;
	private double latitude;

	/**
	 * @return the rid
	 */
	public int getRid() {
		return rid;
	}

	/**
	 * @param rid
	 *            the rid to set
	 */
	public void setRid(int rid) {
		this.rid = rid;
	}

	/**
	 * @return the gid
	 */
	public String getGid() {
		return gid;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * @param gid
	 *            the gid to set
	 */
	public void setGid(String gid) {
		this.gid = gid;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the staddress
	 */
	public String getStaddress() {
		return staddress;
	}

	/**
	 * @param staddress
	 *            the staddress to set
	 */
	public void setStaddress(String staddress) {
		this.staddress = staddress;
	}

	/**
	 * @return the zipcode
	 */
	public int getZipcode() {
		return zipcode;
	}

	/**
	 * @param zipcode
	 *            the zipcode to set
	 */
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}

	public JSONObject toJSON() {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("rid", rid).put("gid", gid).put("name", name).put("staddress", staddress)
				.put("zipcode", zipcode).put("latitude",latitude).put("longitude", longitude);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObj;
	}

	public static Restaurant toRestaurant(String jsonRestaurant) {
		Restaurant restaurant = new Restaurant();
		try {
			JSONObject jsonObj = new JSONObject(jsonRestaurant);
			restaurant.rid = jsonObj.getInt("rid");
			restaurant.gid = jsonObj.getString("gid");
			restaurant.name = jsonObj.getString("name");
			restaurant.staddress = jsonObj.getString("staddress");
			restaurant.zipcode = jsonObj.getInt("zipcode");
			restaurant.latitude = jsonObj.getDouble("latitude");
			restaurant.longitude = jsonObj.getDouble("longitude");
		}
		catch (JSONException e) {
			System.out.println("Couldn't parse JSON " + restaurant.rid);
			if (restaurant.rid <= 0)
				return null;
		}
		return restaurant;
	}
}
