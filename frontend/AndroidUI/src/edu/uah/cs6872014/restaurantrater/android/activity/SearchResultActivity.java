package edu.uah.cs6872014.restaurantrater.android.activity;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import edu.uah.cs6872014.restaurantrater.android.R;
import edu.uah.cs6872014.restaurantrater.android.Utilities.GoogleUtilities;
import edu.uah.cs6872014.restaurantrater.android.Utilities.RESTUtilities;
import edu.uah.cs6872014.restaurantrater.android.activity.adapter.SearchResultAdapter;

/**
 * @author Madhu Sigdel 
 * Activity which displays the restaurant list ( Searched by place name, searched by gps, favorite )
 * 
 */

public class SearchResultActivity extends ListActivity {
	private String	place			= null;
	private String	location		= null;
	private String	jsonResponse	= null;
	private String	favorites		= null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_result);
		Intent intent = getIntent();
		place = intent.getStringExtra("place");
		location = intent.getStringExtra("location");
		favorites = intent.getStringExtra("favorite");

		if (savedInstanceState == null) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			getFragmentManager().beginTransaction().add(R.id.container, fragment).commit();

			if (jsonResponse == null) {

				if (location != null) {
					jsonResponse = GoogleUtilities.getPlacesLocationJSONString(location);
				}
				else if (place != null) {
					jsonResponse = GoogleUtilities.getPlacesJSONString(place);
				}
				else {
					jsonResponse = favorites;
					getActionBar().setTitle("Favorites");
				}

			}
			if (jsonResponse == null) {
				Toast toast = Toast.makeText(getApplicationContext(), "No restaurants found. Please try again",
					Toast.LENGTH_LONG);
				toast.show();
				finish();
				return;
			}
			JSONArray jsonResultsArray = null;
			String[] results = null;
			ArrayList<String> gidArray = new ArrayList<String>();
			try {
				JSONObject jsonResponseObj = new JSONObject(jsonResponse);
				jsonResultsArray = jsonResponseObj.getJSONArray("results");
				results = new String[jsonResultsArray.length()];
				for (int i = 0; i < jsonResultsArray.length(); i++) {
					results[i] = jsonResultsArray.get(i).toString();
					JSONObject resultObj = new JSONObject(results[i]);
					String gid = resultObj.getString("id");
					gidArray.add(gid);
				}
			}

			catch (JSONException e) {
				e.printStackTrace();
			}
			HashMap<String, String> ratingmap = fetchAllRatingGid(gidArray);
			
			SearchResultAdapter adapter = new SearchResultAdapter(this, results, ratingmap);
			setListAdapter(adapter);
			final String[] localResults = results;
			getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> av, View view, int position, long id) {
					Intent intent = new Intent(av.getContext(), RestaurantDetailActivity.class);
					JSONObject restaurantObj = null;
					double lat = 0;
					double lng = 0;
					String gid = null;
					JSONObject restObj = null;
					try {
						restaurantObj = new JSONObject(localResults[position]);
						if (restaurantObj.has("geometry")) {
							JSONObject obj = restaurantObj.getJSONObject("geometry");
							if (obj.has("location"))
								obj = obj.getJSONObject("location");
							if (obj.has("lat")) {
								lat = obj.getDouble("lat");
								lng = obj.getDouble("lng");
							}
						}
						gid = restaurantObj.getString("id");
						restObj = new JSONObject();
						restObj.put("lat", lat).put("lng", lng).put("gid", gid);
					}
					catch (JSONException e) {
						e.printStackTrace();
					}
					intent.putExtra("restObj", restObj.toString());
					startActivity(intent);
				}
			});
		}
	}

	private HashMap<String, String> fetchAllRatingGid(ArrayList<String> gidArray) {
		String stringRating = RESTUtilities.getOverallAvgRating(gidArray);
		if (stringRating == null)
			return null;
		HashMap<String, String> ratingsGid = new HashMap<String, String>();
		try {
			JSONArray jsonRatingGid = new JSONArray(stringRating);
			for (int i = 0; i < jsonRatingGid.length(); i++) {
				String jsonRating = jsonRatingGid.getString(i);
				try {
					JSONObject jsonObj = new JSONObject(jsonRating);
					ratingsGid.put(jsonObj.getString("gid"), jsonObj.getString("avgstar"));

				}
				catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return ratingsGid;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.search_result, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_search_result, container, false);

			return rootView;
		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
		}

		@Override
		public void onSaveInstanceState(Bundle outState) {
			super.onSaveInstanceState(outState);
		}
	}

}
