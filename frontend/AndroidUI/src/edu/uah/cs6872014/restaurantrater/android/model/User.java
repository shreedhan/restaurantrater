/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.android.model;

import java.sql.Timestamp;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Suman
 *
 */
public class User {

	private int			uid;
	private String		username;
	private String		email;
	private String		password;
	private String		staddress;
	private int			zipcode;
	private String		fname;
	private String		lname;
	private String		midname;
	private Timestamp	lastlogints;
	private Timestamp	createdts;

	/**
	 * @return the uid
	 */
	public int getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(int uid) {
		this.uid = uid;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the staddress
	 */
	public String getStaddress() {
		return staddress;
	}

	/**
	 * @param staddress
	 *            the staddress to set
	 */
	public void setStaddress(String staddress) {
		this.staddress = staddress;
	}

	/**
	 * @return the zipcode
	 */
	public int getZipcode() {
		return zipcode;
	}

	/**
	 * @param zipcode
	 *            the zipcode to set
	 */
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}

	/**
	 * @return the fname
	 */
	public String getFname() {
		return fname;
	}

	/**
	 * @param fname
	 *            the fname to set
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}

	/**
	 * @return the lname
	 */
	public String getLname() {
		return lname;
	}

	/**
	 * @param lname
	 *            the lname to set
	 */
	public void setLname(String lname) {
		this.lname = lname;
	}

	/**
	 * @return the midname
	 */
	public String getMidname() {
		return midname;
	}

	/**
	 * @param midname
	 *            the midname to set
	 */
	public void setMidname(String midname) {
		this.midname = midname;
	}

	/**
	 * @return the lastlogints
	 */
	public Timestamp getLastlogints() {
		return lastlogints;
	}

	/**
	 * @param lastlogints
	 *            the lastlogints to set
	 */
	public void setLastlogints(Timestamp lastlogints) {
		this.lastlogints = lastlogints;
	}

	/**
	 * @return the createdts
	 */
	public Timestamp getCreatedts() {
		return createdts;
	}

	/**
	 * @param createdts
	 *            the createdts to set
	 */
	public void setCreatedts(Timestamp createdts) {
		this.createdts = createdts;
	}

	public JSONObject toJSON() {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("uid", uid).put("username", username).put("email", email).put("password", password)
				.put("staddress", staddress).put("zipcode", zipcode).put("fname", fname).put("lname", lname)
				.put("midname", midname).put("lastlogints", lastlogints).put("createdts", createdts);
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObj;
	}

	public static User toUser(String jsonUser) {
		User user = new User();
		try {
			JSONObject jsonObj = new JSONObject(jsonUser);

			user.uid = jsonObj.getInt("uid");
			user.username = jsonObj.getString("username");
			user.email = jsonObj.getString("email");
			user.password = jsonObj.getString("password");
			user.staddress = jsonObj.getString("staddress");
			user.zipcode = jsonObj.getInt("zipcode");
			user.fname = jsonObj.getString("fname");
			user.lname = jsonObj.getString("lname");
			user.midname = jsonObj.getString("midname");
			user.lastlogints = Timestamp.valueOf(jsonObj.getString("lastlogints"));
			user.createdts = Timestamp.valueOf(jsonObj.getString("createdts"));
		}
		catch (JSONException e) {
			System.out.println("Couldn't parse JSON " + user.uid);
			if (user.uid <= 0)
				return null;
		}
		return user;
	}
}
