/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.controller;

import java.sql.SQLException;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import edu.uah.cs6872014.restaurantrater.model.Rate;

/**
 * @author Suman
 *
 */

@Path("/rates")
public class RateService {

	@GET
	@Path("/{uid}&{rid}&{cid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecord(@PathParam("uid") int uid, @PathParam("rid") int rid, @PathParam("cid") int cid) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("uid", Integer.toString(uid));
		keyMap.put("rid", Integer.toString(rid));
		keyMap.put("cid", Integer.toString(cid));

		StringBuilder sb = new StringBuilder();

		String result = null;
		try {
			result = Rate.jsonFindByKey(keyMap);
			if (result == null) {
				return Response.status(404).entity("Requested rate does not exist").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();
	}

	@GET
	@Path("/&{rid}&")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRestaurantRecord(@PathParam("rid") int rid) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("rid", Integer.toString(rid));

		StringBuilder sb = new StringBuilder();

		String result = null;
		try {
			result = Rate.jsonFindByKeyMap(keyMap);
			if (result == null) {
				return Response.status(404).entity("Requested rate does not exist").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();
	}
	
	@GET
	@Path("/{uid}&&")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllUSerRecord(@PathParam("uid") int uid) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("uid", Integer.toString(uid));

		StringBuilder sb = new StringBuilder();

		String result = null;
		try {
			result = Rate.jsonFindByKeyMap(keyMap);
			if (result == null) {
				return Response.status(404).entity("Requested rate does not exist").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();
	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRecords() {
		StringBuilder sb = new StringBuilder();
		String result = null;
		try {
			result = Rate.jsonFindAll();
			if (result == null) {
				return Response.status(404).entity("No rates found").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertRecord(String jsonRate) {
		System.out.println(jsonRate);
		Rate rate = Rate.toRate(jsonRate);
		try {
			rate.save();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while inserting new record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(jsonRate).build();
	}

	@PUT
	@Path("/{uid}&{rid}&{cid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateRecord(@PathParam("uid") int uid, @PathParam("rid") int rid, @PathParam("cid") int cid, String jsonRate) {
		System.out.println(jsonRate);
		Rate rate = Rate.toRate(jsonRate);
		try {
			rate.save();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while updating record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(rate.toJSON().toString()).build();
	}

	@DELETE
	@Path("/{uid}&{rid}&{cid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteRecord(@PathParam("uid") int uid, @PathParam("rid") int rid, @PathParam("cid") int cid) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("uid", Integer.toString(uid));
		keyMap.put("rid", Integer.toString(rid));
		keyMap.put("cid", Integer.toString(cid));
		
		Rate rate = null;
		String jsonRate = null;
		try {
			rate = Rate.findByKey(keyMap);
			if (rate != null) {
				jsonRate = rate.toJSON().toString();
				rate.delete();
			}
			else
				jsonRate = new JSONObject().toString();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while deleting record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(jsonRate).build();
	}
}
