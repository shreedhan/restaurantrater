/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.controller;

import java.sql.SQLException;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import edu.uah.cs6872014.restaurantrater.model.User;

/**
 * @author Madhu Sigdel
 *
 */

@Path("/users")
public class UserService {
	
	@GET
	@Path("/{uid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecord(@PathParam("uid") int uid) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("uid", Integer.toString(uid));

		StringBuilder sb = new StringBuilder();

		String result = null;
		try {
			result = User.jsonFindByKey(keyMap);
			if (result == null) {
				return Response.status(404).entity("Requested user does not exist").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();

	}

	@GET
	@Path("/email/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecordByEmail(@PathParam("email") String email) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("email", email);

		StringBuilder sb = new StringBuilder();
		String result = null;
		try {
			result = User.jsonFindByKey(keyMap);
			if (result == null) {
				return Response.status(404).entity("Requested user does not exist").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();

	}
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRecords() {

		StringBuilder sb = new StringBuilder();

		String result = null;
		try {
			result = User.jsonFindAll();
			if (result == null) {
				return Response.status(404).entity("No user found").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();

	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertRecord(String jsonUser) {
		System.out.println(jsonUser);
		User user = User.toUser(jsonUser);
		try {
			user.save();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while inserting new record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(jsonUser).build();
	}

	@PUT
	@Path("/{uid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateRecord(@PathParam("uid") int uid, String jsonUser) {
		System.out.println(jsonUser);
		User user = User.toUser(jsonUser);
		try {
			user.save();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while updating record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(user.toJSON().toString()).build();
	}

	@DELETE
	@Path("/{uid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteRecord(@PathParam("uid") int uid) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("uid", Integer.toString(uid));
		User user = null;
		String jsonUser = null;
		try {
			user = User.findByKey(keyMap);
			if (user != null) {
				jsonUser = user.toJSON().toString();
				user.delete();
			}
			else
				jsonUser = new JSONObject().toString();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while deleting record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(jsonUser).build();
	}


}
