/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uah.cs6872014.restaurantrater.dbinterface.MySQLDBConnection;

/**
 * @author Suman
 *
 */

@Path("/favorites/uid/")
public class FavoriteRestaurantService {

	@GET
	@Path("{uid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecord(@PathParam("uid") int uid) throws JSONException, SQLException {

		String queryString = "SELECT R.GID AS gid, R.NAME AS name, R.STADDRESS AS address, R.LATITUDE AS lat, R.LONGITUDE AS lng FROM FAVORITE F, RESTAURANT R WHERE F.UID = "
			+ uid + " AND F.RID = R.RID";

		StringBuilder sb = new StringBuilder();
		System.out.println(queryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement statement;
		try {
			statement = connection.prepareStatement(queryString);
			resultSet = statement.executeQuery();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Database not connected or Query Error.").build();
		}

		JSONArray result = new JSONArray();
		while (resultSet.next()) {

			JSONObject jsonObj = new JSONObject();
			JSONObject jsonLocation = new JSONObject();
			JSONObject jsonGeometry = new JSONObject();

			jsonLocation.put("lat", resultSet.getDouble("lat")).put("lng", resultSet.getDouble("lng"));
			jsonGeometry.put("location", jsonLocation);

			jsonObj.put("id", resultSet.getString("gid")).put("name", resultSet.getString("name"))
				.put("formatted_address", resultSet.getString("address")).put("rating", 0)
				.put("geometry", jsonGeometry);
			result.put(jsonObj);
		}
		resultSet.close();

		sb.append(result.toString());
		return Response.status(200).entity(sb.toString()).build();

	}
}
