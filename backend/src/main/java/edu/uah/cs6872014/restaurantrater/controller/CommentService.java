/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.controller;

import java.sql.SQLException;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import edu.uah.cs6872014.restaurantrater.model.Comment;

/**
 * @author Madhu Sigdel
 *
 */

@Path("/comments")
public class CommentService {
	@GET
	@Path("/{uid}&{rid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecord(@PathParam("uid") int uid, @PathParam("rid") int rid ) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("uid", Integer.toString(uid));
		keyMap.put("rid", Integer.toString(rid));
		
		StringBuilder sb = new StringBuilder();

		String result = null;
		try {
			result = Comment.jsonFindByKey(keyMap);
			if (result == null) {
				return Response.status(404).entity("Requested comment data does not exist").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();

	}

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRecords() {

		StringBuilder sb = new StringBuilder();

		String result = null;
		try {
			result = Comment.jsonFindAll();
			if (result == null) {
				return Response.status(404).entity("No comment data found").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();

	}
	
	@GET
	@Path("/&{rid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRestaurnatRecord(@PathParam("rid") int rid ) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("rid", Integer.toString(rid));
		
		StringBuilder sb = new StringBuilder();

		String result = null;
		try {
			result = Comment.jsonFindByKeyMap(keyMap);
			if (result == null) {
				return Response.status(404).entity("Requested comment data does not exist").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();

	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertRecord(String jsonComment) {
		System.out.println(jsonComment);
		Comment comment = Comment.toComment(jsonComment);
		try {
			comment.save();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while inserting new record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(jsonComment).build();
	}

	@PUT
	@Path("/{uid}&{rid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateRecord(@PathParam("uid") int uid,@PathParam("rid") int rid, String jsonComment) {
		System.out.println(jsonComment);
		Comment comment = Comment.toComment(jsonComment);
		try {
			comment.save();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while updating record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(comment.toJSON().toString()).build();
	}

	@DELETE
	@Path("/{uid}&{rid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteRecord(@PathParam("uid") int uid,@PathParam("rid") int rid) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("uid", Integer.toString(uid));
		keyMap.put("rid", Integer.toString(rid));
		Comment comment = null;
		String jsonComment = null;
		try {
			comment = Comment.findByKey(keyMap);
			if (comment != null) {
				jsonComment = comment.toJSON().toString();
				comment.delete();
			}
			else
				jsonComment = new JSONObject().toString();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while deleting record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(jsonComment).build();
	}

}
