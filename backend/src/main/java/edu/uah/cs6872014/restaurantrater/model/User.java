/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uah.cs6872014.restaurantrater.dbinterface.MySQLDBConnection;

/**
 * @author Madhu Sigdel
 * 
 */
public class User {
	private static String	tableName	= "USER";
	private int				uid;
	private String			username;
	private String			email;
	private String			password;
	private String			staddress;
	private int				zipcode;
	private String			fname;
	private String			lname;
	private String			midname;
	private Timestamp		lastlogints;
	private Timestamp		createdts;

	public User() {
	}

	public User(int uid) {
		this.uid = uid;
	}

	public static List<User> findAll() throws SQLException {
		String qryString = "SELECT * FROM " + tableName;
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		List<User> rowList = new ArrayList<User>();
		PreparedStatement statement = connection.prepareStatement(qryString);
		resultSet = statement.executeQuery();
		while (resultSet.next()) {
			User user = new User();
			user.uid = resultSet.getInt("uid");
			user.username = resultSet.getString("username");
			user.email = resultSet.getString("email");
			user.password = resultSet.getString("password");
			user.staddress = resultSet.getString("staddress");
			user.zipcode = resultSet.getInt("zipcode");
			user.fname = resultSet.getString("fname");
			user.lname = resultSet.getString("lname");
			user.midname = resultSet.getString("midname");
			user.lastlogints = resultSet.getTimestamp("lastlogints");
			user.createdts = resultSet.getTimestamp("createdts");
			rowList.add(user);
		}
		resultSet.close();
		return rowList;
	}

	public static User findByKey(Map<String, String> keyMap) throws SQLException {
		String qryString = "SELECT * FROM " + tableName;
		String qryCondition = "";
		if (keyMap.size() > 0) {
			for (Map.Entry<String, String> entry : keyMap.entrySet()) {
				qryCondition = " and " + qryCondition + entry.getKey() + " = '" + entry.getValue() + "'";
			}
			qryCondition = qryCondition.replaceFirst(" and ", "");
			qryCondition = " where " + qryCondition;
			qryString = qryString + qryCondition;
		}
		System.out.println(qryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement statement = connection.prepareStatement(qryString);
		resultSet = statement.executeQuery();
		if (resultSet.next()) {
			User user = new User();
			user.uid = resultSet.getInt("uid");
			user.username = resultSet.getString("username");
			user.email = resultSet.getString("email");
			user.password = resultSet.getString("password");
			user.staddress = resultSet.getString("staddress");
			user.zipcode = resultSet.getInt("zipcode");
			user.fname = resultSet.getString("fname");
			user.lname = resultSet.getString("lname");
			user.midname = resultSet.getString("midname");
			user.lastlogints = resultSet.getTimestamp("lastlogints");
			user.createdts = resultSet.getTimestamp("createdts");
			return user;
		}
		else
			return null;
	}

	public static String jsonFindByKey(HashMap<String, String> keyMap) throws SQLException {
		User user = findByKey(keyMap);
		if (user == null)
			return null;
		JSONObject jsonObj = user.toJSON();
		return jsonObj.toString();
	}

	public static String jsonFindAll() throws SQLException {
		List<User> user = findAll();
		JSONArray array = toJSONArray(user);
		return array.toString();
	}

	public static JSONArray toJSONArray(List<User> user) {
		JSONArray array = new JSONArray();

		for (User single_user : user) {
			JSONObject jsonObj = single_user.toJSON();
			array.put(jsonObj);
		}

		return array;
	}

	public Map<String, String> getPrimaryKey() {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("uid", Integer.toString(this.uid));
		return map;
	}

	public User save() throws SQLException {
		User user = findByKey(this.getPrimaryKey());
		if (user == null) {
			Connection connection = MySQLDBConnection.getConnection();

			String qryString = "INSERT INTO "
				+ tableName
				+ "(USERNAME, EMAIL, PASSWORD, FNAME, MIDNAME, LNAME, ZIPCODE, STADDRESS, LASTLOGINTS, CREATEDTS) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			PreparedStatement statement = connection.prepareStatement(qryString);

			statement.setString(1, this.username);
			statement.setString(2, this.email);
			statement.setString(3, this.password);
			statement.setString(4, this.fname);
			statement.setString(5, this.midname);
			statement.setString(6, this.lname);
			statement.setInt(7, this.zipcode);
			statement.setString(8, this.staddress);
			statement.setTimestamp(9, this.lastlogints);
			statement.setTimestamp(10, this.createdts);
			System.out.println(qryString);
			int result = 0;
			result = statement.executeUpdate();
			if (result == 1) {
				System.out.println("Insert successfully");
			}

		}
		else {
			Connection connection = MySQLDBConnection.getConnection();
			// Update current row here
			String qryString = "UPDATE "
				+ tableName
				+ " SET USERNAME = ?, EMAIL = ?, PASSWORD = ?, FNAME = ?, MIDNAME = ?, LNAME = ?, ZIPCODE = ?, STADDRESS = ?, LASTLOGINTS = ? WHERE UID = "
				+ this.uid;

			PreparedStatement statement = connection.prepareStatement(qryString);
			statement.setString(1, this.username);
			statement.setString(2, this.email);
			statement.setString(3, this.password);
			statement.setString(4, this.fname);
			statement.setString(5, this.midname);
			statement.setString(6, this.lname);
			statement.setInt(7, this.zipcode);
			statement.setString(8, this.staddress);
			statement.setTimestamp(9, this.lastlogints);

			System.out.println(qryString);
			int result = 0;
			result = statement.executeUpdate();
			if (result == 1) {
				System.out.println("Updated successfully");
			}
		}
		return this;
	}

	public User delete() throws SQLException {
		String qryString = "DELETE FROM " + tableName + " where UID = " + this.uid;
		System.out.println(qryString);
		Connection connection = MySQLDBConnection.getConnection();
		int result = 0;
		PreparedStatement statement = connection.prepareStatement(qryString);
		result = statement.executeUpdate();
		if (result == 1) {
			System.out.println("Deleted successfully");
		}
		return this;
	}

	public JSONObject toJSON() {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("uid", uid).put("username", username).put("email", email).put("password", password)
			.put("staddress", staddress).put("zipcode", zipcode).put("fname", fname).put("lname", lname)
			.put("midname", midname).put("lastlogints", lastlogints).put("createdts", createdts);
		return jsonObj;
	}

	public static User toUser(String jsonUser) {
		User user = new User();
		JSONObject jsonObj = new JSONObject(jsonUser);
		try {
			user.uid = jsonObj.getInt("uid");
			user.username = jsonObj.getString("username");
			user.email = jsonObj.getString("email");
			user.password = jsonObj.getString("password");
			user.staddress = jsonObj.getString("staddress");
			user.zipcode = jsonObj.getInt("zipcode");
			user.fname = jsonObj.getString("fname");
			user.lname = jsonObj.getString("lname");
			user.midname = jsonObj.getString("midname");
			user.lastlogints = Timestamp.valueOf(jsonObj.getString("lastlogints"));
			user.createdts = Timestamp.valueOf(jsonObj.getString("createdts"));
		}
		catch (JSONException e) {
			// e.printStackTrace();
			System.out.println("Couldn't parse JSON " + user.uid);
			if (user.uid <= 0)
				return null;
		}
		return user;
	}

	// Getter and Setter methods
	/**
	 * @return the uid
	 */
	public int getUid() {
		return uid;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the staddress
	 */
	public String getStaddress() {
		return staddress;
	}

	/**
	 * @return the zipcode
	 */
	public int getZipcode() {
		return zipcode;
	}

	/**
	 * @return the fname
	 */
	public String getFname() {
		return fname;
	}

	/**
	 * @return the lname
	 */
	public String getLname() {
		return lname;
	}

	/**
	 * @return the midname
	 */
	public String getMidname() {
		return midname;
	}

	/**
	 * @return the lastlogints
	 */
	public Timestamp getLastlogints() {
		return lastlogints;
	}

	/**
	 * @return the createdts
	 */
	public Timestamp getCreatedts() {
		return createdts;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @param staddress
	 *            the staddress to set
	 */
	public void setStaddress(String staddress) {
		this.staddress = staddress;
	}

	/**
	 * @param zipcode
	 *            the zipcode to set
	 */
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}

	/**
	 * @param fname
	 *            the fname to set
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}

	/**
	 * @param lname
	 *            the lname to set
	 */
	public void setLname(String lname) {
		this.lname = lname;
	}

	/**
	 * @param midname
	 *            the midname to set
	 */
	public void setMidname(String midname) {
		this.midname = midname;
	}

	/**
	 * @param lastlogints
	 *            the lastlogints to set
	 */
	public void setLastlogints(Timestamp lastlogints) {
		this.lastlogints = lastlogints;
	}

	/**
	 * @param createdts
	 *            the createdts to set
	 */
	public void setCreatedts(Timestamp createdts) {
		this.createdts = createdts;
	}

	public String getFullName() {
		String fullName = null;
		if (fname != null && !fname.isEmpty())
			fullName = fname;
		if (midname != null && !midname.isEmpty()) {
			if (fullName != null)
				fullName = fullName + " " + midname;
			else
				fullName = midname;
		}
		if (lname != null && !lname.isEmpty()) {
			if (fullName != null)
				fullName = fullName + " " + lname;
			else
				fullName = lname;
		}
		if (fullName == null)
			fullName = username;
		if (fullName == null)
			fullName = "User " + uid;
		return fullName;
	}

}
