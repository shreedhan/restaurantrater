/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.dbinterface;

/**
 * @author shreedhan
 * 
 */
public class Constants {
	public static final String	DB_NAME		= "RESTAURANT_RATER";
	public static final String	HOST		= "localhost";
	public static final String	PORT		= "3306";
	public static final String	DB_USERNAME	= "shreedhan";
	public static final String	DB_PASSWORD	= "shreedhan";
}
