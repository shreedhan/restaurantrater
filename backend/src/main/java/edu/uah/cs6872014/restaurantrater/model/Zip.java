/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uah.cs6872014.restaurantrater.dbinterface.MySQLDBConnection;

/**
 * @author Suman
 * 
 */
public class Zip {

	private int				zipcode;
	private String			city;
	private String			state;

	private static String	tableName	= "ZIP";

	public Zip() {
	}

	public Zip(int zipcode) {
		this.zipcode = zipcode;
	}

	public static List<Zip> findAll() throws SQLException {
		String queryString = "SELECT * FROM " + tableName;
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		List<Zip> rowList = new ArrayList<Zip>();
		PreparedStatement statement = connection.prepareStatement(queryString);
		resultSet = statement.executeQuery();
		while (resultSet.next()) {
			Zip zip = new Zip();
			zip.zipcode = resultSet.getInt("zipcode");
			zip.city = resultSet.getString("city");
			zip.state = resultSet.getString("state");
			rowList.add(zip);
		}
		resultSet.close();
		return rowList;
	}

	public static Zip findByKey(Map<String, String> keyMap) throws SQLException {
		String queryString = "SELECT * FROM " + tableName;
		String queryCondition = "";
		if (keyMap.size() > 0) {
			for (Map.Entry<String, String> entry : keyMap.entrySet()) {
				queryCondition = " AND " + queryCondition + entry.getKey() + " = " + entry.getValue();
			}
			queryCondition = queryCondition.replaceFirst(" AND ", "");
			queryCondition = " where " + queryCondition;
			queryString = queryString + queryCondition;
		}
		System.out.println(queryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement statement = connection.prepareStatement(queryString);
		resultSet = statement.executeQuery();
		if (resultSet.next()) {
			Zip zip = new Zip();
			zip.zipcode = resultSet.getInt("zipcode");
			zip.city = resultSet.getString("city");
			zip.state = resultSet.getString("state");
			return zip;
		}
		else
			return null;
	}

	public static String jsonFindByKey(HashMap<String, String> keyMap) throws SQLException {
		Zip zip = findByKey(keyMap);
		if (zip == null)
			return null;
		JSONObject jsonObj = zip.toJSON();
		return jsonObj.toString();
	}

	public static String jsonFindAll() throws SQLException {
		List<Zip> zips = findAll();
		JSONArray array = toJSONArray(zips);
		return array.toString();
	}

	public Map<String, String> getPrimaryKey() {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("zipcode", Integer.toString(this.zipcode));
		return map;
	}

	public Zip save() throws SQLException {
		Zip zip = findByKey(this.getPrimaryKey());
		if (zip == null) {
			String queryString = "INSERT INTO " + tableName + "(ZIPCODE, CITY, STATE) VALUES (" + this.zipcode + ",'"
				+ this.state + "','" + this.state + "')";
			System.out.println(queryString);
			Connection connection = MySQLDBConnection.getConnection();
			int result = 0;
			PreparedStatement statement = connection.prepareStatement(queryString);
			result = statement.executeUpdate();
			if (result == 1) {
				System.out.println("Insert successfully");
			}
		}
		else {
			String queryString = "UPDATE " + tableName + " SET CITY = '" + this.city + "', STATE = '" + this.state
				+ "' WHERE ZIPCODE = " + this.zipcode;
			System.out.println(queryString);
			Connection connection = MySQLDBConnection.getConnection();
			int result = 0;
			PreparedStatement statement = connection.prepareStatement(queryString);
			result = statement.executeUpdate();
			if (result == 1) {
				System.out.println("Updated successfully");
			}
		}
		return this;
	}

	public Zip delete() throws SQLException {
		String queryString = "DELETE FROM " + tableName + " where ZIPCODE = " + this.zipcode;
		System.out.println(queryString);
		Connection connection = MySQLDBConnection.getConnection();
		int result = 0;
		PreparedStatement statement = connection.prepareStatement(queryString);
		result = statement.executeUpdate();
		if (result == 1) {
			System.out.println("Deleted successfully");
		}
		return this;
	}

	public JSONObject toJSON() {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("zipcode", zipcode).put("city", city).put("state", state);
		return jsonObj;
	}

	public static JSONArray toJSONArray(List<Zip> zips) {
		JSONArray array = new JSONArray();
		for (Zip zip : zips) {
			JSONObject jsonObj = zip.toJSON();
			array.put(jsonObj);
		}
		return array;
	}

	public static Zip toZip(String jsonZip) {
		Zip zip = new Zip();
		JSONObject jsonObj = new JSONObject(jsonZip);

		try {
			zip.zipcode = jsonObj.getInt("zipcode");
			zip.city = jsonObj.getString("city");
			zip.state = jsonObj.getString("state");

		}
		catch (JSONException e) {
			System.out.println("Couldn't parse JSON " + zip.zipcode);
			if (zip.zipcode <= 0)
				return null;
		}
		return zip;
	}

	/**
	 * @return the zipcode
	 */
	public int getZipcode() {
		return zipcode;
	}

	/**
	 * @param zipcode
	 *            the zipcode to set
	 */
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

}
