/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uah.cs6872014.restaurantrater.dbinterface.MySQLDBConnection;

/**
 * @author Madhu Sigdel
 * 
 */
public class Favorite {
	private static String	tableName	= "FAVORITE";
	private int				rid;
	private int				uid;
	private Timestamp		createdts;

	public Favorite() {
	}

	public Favorite(int uid, int rid) {
		this.uid = uid;
		this.rid = rid;
	}

	public static List<Favorite> findAll() throws SQLException {
		String qryString = "SELECT * FROM " + tableName;
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		List<Favorite> rowList = new ArrayList<Favorite>();
		PreparedStatement statement = connection.prepareStatement(qryString);
		resultSet = statement.executeQuery();
		while (resultSet.next()) {
			Favorite favorite = new Favorite();
			favorite.uid = resultSet.getInt("uid");
			favorite.rid = resultSet.getInt("rid");
			favorite.createdts = resultSet.getTimestamp("createdts");
			rowList.add(favorite);
		}
		resultSet.close();
		return rowList;
	}

	public static List<Favorite> findByKeyMap(Map<String, String> keyMap) throws SQLException {
		String qryString = "SELECT * FROM " + tableName + " WHERE 1=1 ";
		String qryCondition = "";
		if (keyMap.size() > 0) {
			for (Map.Entry<String, String> entry : keyMap.entrySet()) {
				qryCondition = qryCondition + " and " + entry.getKey() + " = " + entry.getValue();
			}
			qryString = qryString + qryCondition;
		}
		System.out.println(qryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		List<Favorite> rowList = new ArrayList<Favorite>();
		PreparedStatement statement = connection.prepareStatement(qryString);
		resultSet = statement.executeQuery();
		while (resultSet.next()) {
			Favorite favorite = new Favorite();
			favorite.uid = Integer.parseInt(resultSet.getObject("uid").toString());
			favorite.rid = Integer.parseInt(resultSet.getObject("rid").toString());
			favorite.createdts = Timestamp.valueOf(resultSet.getObject("createdts").toString());
			rowList.add(favorite);
		}
		resultSet.close();
		return rowList;
	}

	public static Favorite findByKey(Map<String, String> keyMap) throws SQLException {
		String qryString = "SELECT * FROM " + tableName + " WHERE 1=1 ";
		String qryCondition = "";
		if (keyMap.size() > 0) {
			for (Map.Entry<String, String> entry : keyMap.entrySet()) {
				qryCondition = qryCondition + " and " + entry.getKey() + " = " + entry.getValue();
			}
			qryString = qryString + qryCondition;
		}
		System.out.println(qryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement statement = connection.prepareStatement(qryString);
		resultSet = statement.executeQuery();
		if (resultSet.next()) {
			Favorite favorite = new Favorite();
			favorite.uid = Integer.parseInt(resultSet.getObject("uid").toString());
			favorite.rid = Integer.parseInt(resultSet.getObject("rid").toString());
			favorite.createdts = Timestamp.valueOf(resultSet.getObject("createdts").toString());
			return favorite;
		}
		else
			return null;
	}

	public static String jsonFindByKey(HashMap<String, String> keyMap) throws SQLException {
		Favorite favorite = findByKey(keyMap);
		if (favorite == null)
			return null;
		JSONObject jsonObj = favorite.toJSON();
		return jsonObj.toString();
	}

	public static String jsonFindAll() throws SQLException {
		List<Favorite> favorites = findAll();
		JSONArray array = toJSONArray(favorites);
		return array.toString();
	}

	public static String jsonFindByKeyMap(HashMap<String, String> keyMap) throws SQLException {
		List<Favorite> favorites = findByKeyMap(keyMap);
		JSONArray array = toJSONArray(favorites);
		return array.toString();
	}

	public static JSONArray toJSONArray(List<Favorite> favorites) {
		JSONArray array = new JSONArray();

		for (Favorite favorite : favorites) {
			JSONObject jsonObj = favorite.toJSON();
			array.put(jsonObj);
		}

		return array;
	}

	public Map<String, String> getPrimaryKey() {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("uid", Integer.toString(this.uid));
		map.put("rid", Integer.toString(this.rid));
		return map;
	}

	public Favorite save() throws SQLException {
		Favorite favorite = findByKey(this.getPrimaryKey());
		if (favorite == null) {
			String qryString = "INSERT INTO " + tableName + "(UID, RID, CREATEDTS) VALUES (" + this.uid + ","
				+ this.rid + ",'" + this.createdts + "')";
			System.out.println(qryString);
			Connection connection = MySQLDBConnection.getConnection();
			int result = 0;
			PreparedStatement statement = connection.prepareStatement(qryString);
			result = statement.executeUpdate();
			if (result == 1) {
				System.out.println("Insert successfully");
			}

		}
		else {
			// Update current row here
			String qryString = "UPDATE " + tableName + " SET CREATEDTS = '" + this.createdts + "' WHERE UID = "
				+ this.uid + " AND RID = " + this.rid;
			System.out.println(qryString);
			Connection connection = MySQLDBConnection.getConnection();
			int result = 0;
			PreparedStatement statement = connection.prepareStatement(qryString);
			result = statement.executeUpdate();
			if (result == 1) {
				System.out.println("Updated successfully");
			}
		}
		return this;
	}

	public Favorite delete() throws SQLException {
		String qryString = "DELETE FROM " + tableName + " WHERE UID = " + this.uid + " AND RID = " + this.rid;
		System.out.println(qryString);
		Connection connection = MySQLDBConnection.getConnection();
		int result = 0;
		PreparedStatement statement = connection.prepareStatement(qryString);
		result = statement.executeUpdate();
		if (result == 1) {
			System.out.println("Deleted successfully");
		}
		return this;
	}

	public JSONObject toJSON() {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("uid", uid).put("rid", rid).put("createdts", createdts);
		return jsonObj;
	}

	public static Favorite toFavorite(String jsonFavorite) {
		Favorite favorite = new Favorite();
		JSONObject jsonObj = new JSONObject(jsonFavorite);
		try {
			favorite.uid = jsonObj.getInt("uid");
			favorite.rid = jsonObj.getInt("rid");
			favorite.createdts = Timestamp.valueOf(jsonObj.getString("createdts"));
		}
		catch (JSONException e) {
			// e.printStackTrace();
			System.out.println("Couldn't parse JSON for uid : " + favorite.uid + " & rid : " + favorite.rid);
			if (favorite.uid <= 0 || favorite.rid <= 0)
				return null;
		}
		return favorite;
	}

	/**
	 * @return the rid
	 */
	public int getRid() {
		return rid;
	}

	/**
	 * @param rid
	 *            the rid to set
	 */
	public void setRid(int rid) {
		this.rid = rid;
	}

	/**
	 * @return the uid
	 */
	public int getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(int uid) {
		this.uid = uid;
	}

	/**
	 * @return the createdts
	 */
	public Timestamp getCreatedts() {
		return createdts;
	}

	/**
	 * @param createdts
	 *            the createdts to set
	 */
	public void setCreatedts(Timestamp createdts) {
		this.createdts = createdts;
	}

}
