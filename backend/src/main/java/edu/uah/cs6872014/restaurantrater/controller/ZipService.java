/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.controller;

import java.sql.SQLException;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import edu.uah.cs6872014.restaurantrater.model.Restaurant;
import edu.uah.cs6872014.restaurantrater.model.Zip;

/**
 * @author Suman
 *
 */
@Path("/zips")
public class ZipService {

	@GET
	@Path("/{zipcode}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecord(@PathParam("zipcode") int zipcode) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("zipcode", Integer.toString(zipcode));

		StringBuilder sb = new StringBuilder();

		String result = null;
		try {
			result = Restaurant.jsonFindByKey(keyMap);
			if (result == null) {
				return Response.status(404).entity("Requested zip does not exist").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();
	}

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRecords() {
		StringBuilder sb = new StringBuilder();
		String result = null;
		try {
			result = Zip.jsonFindAll();
			if (result == null) {
				return Response.status(404).entity("No zips found").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertRecord(String jsonZip) {
		System.out.println(jsonZip);
		Zip zip = Zip.toZip(jsonZip);
		try {
			zip.save();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while inserting new record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(jsonZip).build();
	}

	@PUT
	@Path("/{zipcode}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateRecord(@PathParam("zipcode") int zipcode, String jsonZip) {
		System.out.println(jsonZip);
		Zip zip = Zip.toZip(jsonZip);
		try {
			zip.save();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while updating record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(zip.toJSON().toString()).build();
	}

	@DELETE
	@Path("/{zipcode}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteRecord(@PathParam("zipcode") int zipcode) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("zipcode", Integer.toString(zipcode));
		Zip zip = null;
		String jsonZip = null;
		try {
			zip = Zip.findByKey(keyMap);
			if (zip != null) {
				jsonZip = zip.toJSON().toString();
				zip.delete();
			}
			else
				jsonZip = new JSONObject().toString();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while deleting record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(jsonZip).build();
	}
}
