/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uah.cs6872014.restaurantrater.dbinterface.MySQLDBConnection;

/**
 * @author Suman
 * 
 */
public class Restaurant {

	private int				rid;
	private String			gid;
	private String			name;
	private String			staddress;
	private int				zipcode;
	private double			latitude;
	private double			longitude;

	private static String	tableName	= "RESTAURANT";

	public Restaurant() {
	}

	public Restaurant(int rid) {
		this.rid = rid;
	}

	public static List<Restaurant> findAll() throws SQLException {
		String queryString = "SELECT * FROM " + tableName;
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		List<Restaurant> rowList = new ArrayList<Restaurant>();

		PreparedStatement statement = connection.prepareStatement(queryString);
		resultSet = statement.executeQuery();

		while (resultSet.next()) {
			Restaurant restaurant = new Restaurant();
			restaurant.rid = resultSet.getInt("rid");
			restaurant.gid = resultSet.getString("gid");
			restaurant.name = resultSet.getString("name");
			restaurant.staddress = resultSet.getString("staddress");
			restaurant.zipcode = resultSet.getInt("zipcode");
			restaurant.latitude = resultSet.getDouble("latitude");
			restaurant.longitude = resultSet.getDouble("longitude");

			rowList.add(restaurant);
		}
		resultSet.close();
		return rowList;
	}

	public static Restaurant findByKey(Map<String, String> keyMap) throws SQLException {
		String queryString = "SELECT * FROM " + tableName;
		String queryCondition = "";
		if (keyMap.size() > 0) {
			for (Map.Entry<String, String> entry : keyMap.entrySet()) {
				queryCondition = " AND " + queryCondition + entry.getKey() + " = '" + entry.getValue() + "' ";
			}
			queryCondition = queryCondition.replaceFirst(" AND ", "");
			queryCondition = " WHERE " + queryCondition;
			queryString = queryString + queryCondition;
		}
		System.out.println(queryString);

		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement statement = connection.prepareStatement(queryString);
		resultSet = statement.executeQuery();
		if (resultSet.next()) {
			Restaurant restaurant = new Restaurant();
			restaurant.rid = resultSet.getInt("rid");
			restaurant.gid = resultSet.getString("gid");
			restaurant.name = resultSet.getString("name");
			restaurant.staddress = resultSet.getString("staddress");
			restaurant.zipcode = resultSet.getInt("zipcode");
			restaurant.latitude = resultSet.getDouble("latitude");
			restaurant.longitude = resultSet.getDouble("longitude");
			return restaurant;
		}
		else
			return null;
	}

	public static String jsonFindByKey(HashMap<String, String> keyMap) throws SQLException {
		Restaurant restaurant = findByKey(keyMap);
		if (restaurant == null)
			return null;
		JSONObject jsonObj = restaurant.toJSON();
		return jsonObj.toString();
	}

	public static String jsonFindAll() throws SQLException {
		List<Restaurant> restaurants = findAll();
		JSONArray array = toJSONArray(restaurants);
		return array.toString();
	}

	public Map<String, String> getPrimaryKey() {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("rid", Integer.toString(this.rid));
		return map;
	}

	public Restaurant save() throws SQLException {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("gid", this.gid);
		Restaurant restaurant = findByKey(map);
		if (restaurant == null) {

			int result = 0;

			Connection connection = MySQLDBConnection.getConnection();
			String queryString = "INSERT INTO " + tableName
				+ "(NAME, STADDRESS, ZIPCODE, GID, LATITUDE, LONGITUDE) VALUES (?,?,?,?,?,?)";
			PreparedStatement statement = connection.prepareStatement(queryString, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, this.name);
			statement.setString(2, this.staddress);
			statement.setInt(3, this.zipcode);
			statement.setString(4, this.gid);
			statement.setDouble(5, this.latitude);
			statement.setDouble(6, this.longitude);

			result = statement.executeUpdate();
			System.out.println(statement.toString());

			if (result == 1) {
				int rid = -1;
				ResultSet rs = statement.getGeneratedKeys();
				if (rs.next()) {
					rid = rs.getInt(1);
					this.rid = rid;
					System.out.println("Insert successfully");
				}
				else
					return null;
			}
		}
		else {
			int result = 0;
			Connection connection = MySQLDBConnection.getConnection();
			String queryString = "UPDATE " + tableName
				+ " SET NAME = ?, STADDRESS = ?, ZIPCODE = ?, GID = ?, LATITUDE = ?, LONGITUDE = ? WHERE RID = "
				+ this.rid;
			PreparedStatement statement = connection.prepareStatement(queryString);
			statement.setString(1, this.name);
			statement.setString(2, this.staddress);
			statement.setInt(3, this.zipcode);
			statement.setString(4, this.gid);
			statement.setDouble(5, this.latitude);
			statement.setDouble(6, this.longitude);

			System.out.println(queryString);
			result = statement.executeUpdate();

			if (result == 1) {
				System.out.println("Updated successfully");
			}
		}

		return this;
	}

	public Restaurant delete() throws SQLException {
		String queryString = "DELETE FROM " + tableName + " where RID = " + this.rid;
		System.out.println(queryString);
		Connection connection = MySQLDBConnection.getConnection();
		int result = 0;
		PreparedStatement statement = connection.prepareStatement(queryString);
		result = statement.executeUpdate();
		if (result == 1) {
			System.out.println("Deleted successfully");
		}
		return this;
	}

	public JSONObject toJSON() {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("rid", rid).put("gid", gid).put("name", name).put("staddress", staddress).put("zipcode", zipcode)
			.put("longitude", longitude).put("latitude", latitude);
		return jsonObj;
	}

	public static JSONArray toJSONArray(List<Restaurant> restaurants) {
		JSONArray array = new JSONArray();
		for (Restaurant restaurant : restaurants) {
			JSONObject jsonObj = restaurant.toJSON();
			array.put(jsonObj);
		}
		return array;
	}

	public static Restaurant toRestaurant(String jsonRestaurant) {
		Restaurant restaurant = new Restaurant();

		try {
			// restaurant.rid = jsonObj.getInt("rid");
			JSONObject jsonObj = new JSONObject(jsonRestaurant);
			restaurant.gid = jsonObj.getString("gid");
			restaurant.name = jsonObj.getString("name");
			restaurant.staddress = jsonObj.getString("staddress");
			restaurant.zipcode = jsonObj.getInt("zipcode");
			restaurant.latitude = jsonObj.getDouble("latitude");
			restaurant.longitude = jsonObj.getDouble("longitude");
		}
		catch (JSONException e) {
			System.out.println("Couldn't parse JSON ");
			// if (restaurant.rid <= 0)
			// return null;
		}
		return restaurant;
	}

	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the rid
	 */
	public int getRid() {
		return rid;
	}

	/**
	 * @return the gid
	 */
	public String getGid() {
		return gid;
	}

	/**
	 * @param gid
	 *            the gid to set
	 */
	public void setGid(String gid) {
		this.gid = gid;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the staddress
	 */
	public String getStaddress() {
		return staddress;
	}

	/**
	 * @param staddress
	 *            the staddress to set
	 */
	public void setStaddress(String staddress) {
		this.staddress = staddress;
	}

	/**
	 * @return the zipcode
	 */
	public int getZipcode() {
		return zipcode;
	}

	/**
	 * @param zipcode
	 *            the zipcode to set
	 */
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}

}
