/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.model;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uah.cs6872014.restaurantrater.dbinterface.MySQLDBConnection;

/**
 * @author shreedhan
 * 
 */
public class Category implements Serializable{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 4854125475669822139L;
	private int					cid;
	private String				name;
	private String				description;

	private static final String	tableName	= "CATEGORY";

	public Category() {
	}

	public Category(int cid) {
		this.cid = cid;
	}

	public static List<Category> findAll() throws SQLException {
		String qryString = "SELECT * FROM " + tableName;
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		List<Category> rowList = new ArrayList<Category>();
		PreparedStatement statement = connection.prepareStatement(qryString);
		resultSet = statement.executeQuery();
		while (resultSet.next()) {
			Category category = new Category();
			category.cid = resultSet.getInt("cid");
			category.name = resultSet.getString("name");
			category.description = resultSet.getString("description");
			rowList.add(category);
		}
		resultSet.close();
		return rowList;
	}

	public static Category findByKey(Map<String, String> keyMap) throws SQLException {
		String qryString = "SELECT * FROM " + tableName;
		String qryCondition = "";
		if (keyMap.size() > 0) {
			for (Map.Entry<String, String> entry : keyMap.entrySet()) {
				qryCondition = " and " + qryCondition + entry.getKey() + " = " + entry.getValue();
			}
			qryCondition = qryCondition.replaceFirst(" and ", "");
			qryCondition = " where " + qryCondition;
			qryString = qryString + qryCondition;
		}
		System.out.println(qryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement statement = connection.prepareStatement(qryString);
		resultSet = statement.executeQuery();
		if (resultSet.next()) {
			Category category = new Category();
			category.cid = resultSet.getInt("cid");
			category.name = resultSet.getString("name");
			category.description = resultSet.getString("description");
			return category;
		}
		else
			return null;
	}

	public static String jsonFindByKey(HashMap<String, String> keyMap) throws SQLException {
		Category category = findByKey(keyMap);
		if (category == null)
			return null;
		JSONObject jsonObj = category.toJSON();
		return jsonObj.toString();
	}

	public static String jsonFindAll() throws SQLException {
		List<Category> categories = findAll();
		JSONArray array = toJSONArray(categories);
		return array.toString();
	}

	public static JSONArray toJSONArray(List<Category> categories) {
		JSONArray array = new JSONArray();

		for (Category category : categories) {
			JSONObject jsonObj = category.toJSON();
			array.put(jsonObj);
		}

		return array;
	}

	public Map<String, String> getPrimaryKey() {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("cid", Integer.toString(this.cid));
		return map;
	}

	public Category save() throws SQLException {
		Category category = findByKey(this.getPrimaryKey());
		if (category == null) {
			Connection connection = MySQLDBConnection.getConnection();
			int result = 0;
			
			String qryString = "INSERT INTO " + tableName + "(NAME, DESCRIPTION) VALUES (?, ?)";
			PreparedStatement statement = connection.prepareStatement(qryString);
			statement.setString(1, this.name);
			statement.setString(2, this.description);
			
			System.out.println(statement.toString());
			result = statement.executeUpdate();
			if (result == 1) {
				System.out.println("Insert successfully");
			}

		}
		else {
			// Update current row here
			String qryString = "UPDATE " + tableName + " SET NAME = '" + this.name + "', DESCRIPTION = '"
				+ this.description + "' WHERE CID = " + this.cid;
			System.out.println(qryString);
			Connection connection = MySQLDBConnection.getConnection();
			int result = 0;
			PreparedStatement statement = connection.prepareStatement(qryString);
			result = statement.executeUpdate();
			if (result == 1) {
				System.out.println("Updated successfully");
			}
		}
		return this;
	}

	public Category delete() throws SQLException {
		String qryString = "DELETE FROM " + tableName + " where CID = " + this.cid;
		System.out.println(qryString);
		Connection connection = MySQLDBConnection.getConnection();
		int result = 0;
		PreparedStatement statement = connection.prepareStatement(qryString);
		result = statement.executeUpdate();
		if (result == 1) {
			System.out.println("Deleted successfully");
		}
		return this;
	}

	public JSONObject toJSON() {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("cid", cid).put("name", name).put("description", description);
		return jsonObj;
	}

	public static Category toCategory(String jsonCategory) {
		Category category = new Category();
		JSONObject jsonObj = new JSONObject(jsonCategory);
		try {
			category.cid = jsonObj.getInt("cid");
			category.name = jsonObj.getString("name");
			category.description = jsonObj.getString("description");
		}
		catch (JSONException e) {
			// e.printStackTrace();
			System.out.println("Couldn't parse JSON " + category.cid);
			if (category.cid <= 0)
				return null;
		}
		return category;
	}

	// Getter and Setter methods
	/**
	 * @return the cid
	 */
	public int getCid() {
		return cid;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public static JSONObject jsonCountAll() {
		String qryString = "SELECT COUNT(*) AS COUNT FROM " + tableName;
		System.out.println(qryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement statement;
		try {
			statement = connection.prepareStatement(qryString);
			resultSet = statement.executeQuery();
			JSONObject jsonObject = null;
			if (resultSet.next()) {
				String count = resultSet.getString("count");
				jsonObject = new JSONObject();
				jsonObject.put("count", count);
			}
			return jsonObject;
		}
		catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
