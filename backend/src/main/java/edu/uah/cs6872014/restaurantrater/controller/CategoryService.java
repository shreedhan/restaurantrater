/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.controller;

import java.sql.SQLException;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import edu.uah.cs6872014.restaurantrater.model.Category;

/**
 * @author shreedhan
 * 
 */

@Path("/categories")
public class CategoryService {

	@GET
	@Path("/{cid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecord(@PathParam("cid") int cid) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("cid", Integer.toString(cid));

		StringBuilder sb = new StringBuilder();

		String result = null;
		try {
			result = Category.jsonFindByKey(keyMap);
			if (result == null) {
				return Response.status(404).entity("Requested category does not exist").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();

	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRecords() {

		StringBuilder sb = new StringBuilder();

		String result = null;
		try {
			result = Category.jsonFindAll();
			if (result == null) {
				return Response.status(404).entity("No categories found").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();

	}


	@GET
	@Path("/count")
	@Produces(MediaType.APPLICATION_JSON)
	public Response countAllRecords() {

		StringBuilder sb = new StringBuilder();

		JSONObject result = null;
		result = Category.jsonCountAll();
		if (result == null) {
			return Response.status(404).entity("No categories found").build();
		}
		sb.append(result.toString());

		return Response.status(200).entity(sb.toString()).build();

	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertRecord(String jsonCategory) {
		System.out.println(jsonCategory);
		Category category = Category.toCategory(jsonCategory);
		try {
			category.save();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while inserting new record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(jsonCategory).build();
	}

	@PUT
	@Path("/{cid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateRecord(@PathParam("cid") int cid, String jsonCategory) {
		System.out.println(jsonCategory);
		Category category = Category.toCategory(jsonCategory);
		try {
			category.save();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while updating record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(category.toJSON()).build();
	}

	@DELETE
	@Path("/{cid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteRecord(@PathParam("cid") int cid) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("cid", Integer.toString(cid));
		Category category = null;
		String jsonCategory = null;
		try {
			category = Category.findByKey(keyMap);
			if (category != null) {
				jsonCategory = category.toJSON().toString();
				category.delete();
			}
			else
				jsonCategory = new JSONObject().toString();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while deleting record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(jsonCategory).build();
	}

}
