/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.dbinterface;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author shreedhan
 * 
 */
public class MySQLDBConnection {
	private static Connection	connection	= null;

	public static Connection getConnection() {
		if (connection == null)
			setupConnection();
		return connection;
	}

	private static void setupConnection() {
		String url = "jdbc:mysql://" + Constants.HOST + ":" + Constants.PORT + "/" + Constants.DB_NAME
			+ "?zeroDateTimeBehavior=convertToNull";
		try {
			Class.forName("com.mysql.jdbc.Driver");
		}
		catch (ClassNotFoundException e) {
			System.out.println("JDBC Driver not loaded. Check your configuration");
			return;
		}

		try {
			connection = DriverManager.getConnection(url, Constants.DB_USERNAME, Constants.DB_PASSWORD);
		}
		catch (SQLException e) {
			System.out.println("Cannot connect to the database");
			return;
		}

		if (connection == null) {
			System.out.println("Cannot open a connection");
			return;
		}
	}
}
