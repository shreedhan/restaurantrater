/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uah.cs6872014.restaurantrater.dbinterface.MySQLDBConnection;

/**
 * @author Madhu Sigdel
 * 
 */
public class Comment {
	private static String	tableName	= "COMMENT";
	private int				rid;
	private int				uid;
	private String			comment;
	private Timestamp		createdts;
	private Timestamp		modifiedts;

	public Comment() {
	}

	public Comment(int uid, int rid) {
		this.uid = uid;
		this.rid = rid;
	}

	public static List<Comment> findAll() throws SQLException {
		String qryString = "SELECT * FROM " + tableName;
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		List<Comment> rowList = new ArrayList<Comment>();
		PreparedStatement statement = connection.prepareStatement(qryString);
		resultSet = statement.executeQuery();
		while (resultSet.next()) {
			Comment comment = new Comment();
			comment.uid = resultSet.getInt("uid");
			comment.rid = resultSet.getInt("rid");
			comment.comment = resultSet.getString("comment");
			comment.createdts = resultSet.getTimestamp("createdts");
			comment.modifiedts = resultSet.getTimestamp("modifiedts");
			rowList.add(comment);
		}
		resultSet.close();
		return rowList;
	}

	public static List<Comment> findByKeyMap(Map<String, String> keyMap) throws SQLException {
		String qryString = "SELECT * FROM " + tableName + " WHERE 1=1 ";
		String qryCondition = "";
		if (keyMap.size() > 0) {
			for (Map.Entry<String, String> entry : keyMap.entrySet()) {
				qryCondition = qryCondition + " and " + entry.getKey() + " = " + entry.getValue();
			}
			qryString = qryString + qryCondition;
		}
		System.out.println(qryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		List<Comment> rowList = new ArrayList<Comment>();
		PreparedStatement statement = connection.prepareStatement(qryString);
		resultSet = statement.executeQuery();
		while (resultSet.next()) {
			Comment comment = new Comment();
			comment.uid = resultSet.getInt("uid");
			comment.rid = resultSet.getInt("rid");
			comment.comment = resultSet.getString("comment");
			comment.createdts = resultSet.getTimestamp("createdts");
			comment.modifiedts = resultSet.getTimestamp("modifiedts");
			rowList.add(comment);
		}
		resultSet.close();
		return rowList;
	}

	public static Comment findByKey(Map<String, String> keyMap) throws SQLException {
		String qryString = "SELECT * FROM " + tableName + " WHERE 1=1 ";
		String qryCondition = "";
		if (keyMap.size() > 0) {
			for (Map.Entry<String, String> entry : keyMap.entrySet()) {
				qryCondition = qryCondition + " and " + entry.getKey() + " = " + entry.getValue();
			}
			qryString = qryString + qryCondition;
		}
		System.out.println(qryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement statement = connection.prepareStatement(qryString);
		resultSet = statement.executeQuery();
		if (resultSet.next()) {
			Comment comment = new Comment();
			comment.uid = Integer.parseInt(resultSet.getObject("uid").toString());
			comment.rid = Integer.parseInt(resultSet.getObject("rid").toString());
			comment.comment = resultSet.getObject("comment").toString();
			comment.createdts = Timestamp.valueOf(resultSet.getObject("createdts").toString());
			comment.modifiedts = Timestamp.valueOf(resultSet.getObject("modifiedts").toString());
			return comment;
		}
		else
			return null;
	}

	public static String jsonFindByKey(HashMap<String, String> keyMap) throws SQLException {
		Comment comment = findByKey(keyMap);
		if (comment == null)
			return null;
		JSONObject jsonObj = comment.toJSON();
		return jsonObj.toString();
	}

	public static String jsonFindByKeyMap(HashMap<String, String> keyMap) throws SQLException {
		List<Comment> comments = findByKeyMap(keyMap);
		JSONArray array = toJSONArray(comments);
		return array.toString();
	}

	public static String jsonFindAll() throws SQLException {
		List<Comment> comments = findAll();
		JSONArray array = toJSONArray(comments);
		return array.toString();
	}

	public static JSONArray toJSONArray(List<Comment> comments) {
		JSONArray array = new JSONArray();

		for (Comment comment : comments) {
			JSONObject jsonObj = comment.toJSON();
			array.put(jsonObj);
		}

		return array;
	}

	public Map<String, String> getPrimaryKey() {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("uid", Integer.toString(this.uid));
		map.put("rid", Integer.toString(this.rid));
		return map;
	}

	public Comment save() throws SQLException {
		Comment comment = findByKey(this.getPrimaryKey());
		if (comment == null) {
			Connection connection = MySQLDBConnection.getConnection();
			int result = 0;
			String qryString = "INSERT INTO " + tableName
				+ "(UID, RID, COMMENT, CREATEDTS, MODIFIEDTS) VALUES (?,?,?,?,?)";
			System.out.println(qryString);
			PreparedStatement statement = connection.prepareStatement(qryString);
			statement.setInt(1, this.uid);
			statement.setInt(2, this.rid);
			statement.setString(3, this.comment);
			statement.setTimestamp(4, this.createdts);
			statement.setTimestamp(5, this.modifiedts);

			result = statement.executeUpdate();
			if (result == 1) {
				System.out.println("Insert successfully");
			}

		}
		else {
			// Update current row here
			Connection connection = MySQLDBConnection.getConnection();
			int result = 0;

			String qryString = "UPDATE " + tableName + " SET COMMENT = ?, MODIFIEDTS = ? WHERE UID = "
				+ this.uid + " AND RID = " + this.rid;
			System.out.println(qryString);
			PreparedStatement statement = connection.prepareStatement(qryString);
			statement.setString(1, this.comment);
			statement.setString(2, this.modifiedts.toString());

			result = statement.executeUpdate();
			if (result == 1) {
				System.out.println("Updated successfully");
			}
		}
		return this;
	}

	public Comment delete() throws SQLException {
		String qryString = "DELETE FROM " + tableName + " WHERE UID = " + this.uid + " AND RID = " + this.rid;
		System.out.println(qryString);
		Connection connection = MySQLDBConnection.getConnection();
		int result = 0;
		PreparedStatement statement = connection.prepareStatement(qryString);
		result = statement.executeUpdate();
		if (result == 1) {
			System.out.println("Deleted successfully");
		}
		return this;
	}

	public JSONObject toJSON() {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("uid", uid).put("rid", rid).put("comment", comment).put("createdts", createdts)
			.put("modifiedts", modifiedts);
		return jsonObj;
	}

	public static Comment toComment(String jsonComment) {
		Comment comment = new Comment();
		JSONObject jsonObj = new JSONObject(jsonComment);
		try {
			comment.uid = jsonObj.getInt("uid");
			comment.rid = jsonObj.getInt("rid");
			comment.comment = jsonObj.getString("comment");
			comment.createdts = Timestamp.valueOf(jsonObj.getString("createdts"));
			comment.modifiedts = Timestamp.valueOf(jsonObj.getString("modifiedts"));
		}
		catch (JSONException e) {
			// e.printStackTrace();
			System.out.println("Couldn't parse JSON for uid : " + comment.uid + " & rid : " + comment.rid);
			if (comment.uid <= 0 || comment.rid <= 0)
				return null;
		}
		return comment;
	}

	/**
	 * @return the rid
	 */
	public int getRid() {
		return rid;
	}

	/**
	 * @param rid
	 *            the rid to set
	 */
	public void setRid(int rid) {
		this.rid = rid;
	}

	/**
	 * @return the uid
	 */
	public int getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(int uid) {
		this.uid = uid;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the createdts
	 */
	public Timestamp getCreatedts() {
		return createdts;
	}

	/**
	 * @param createdts
	 *            the createdts to set
	 */
	public void setCreatedts(Timestamp createdts) {
		this.createdts = createdts;
	}

	/**
	 * @return the modifiedts
	 */
	public Timestamp getModifiedts() {
		return modifiedts;
	}

	/**
	 * @param modifiedts
	 *            the modifiedts to set
	 */
	public void setModifiedts(Timestamp modifiedts) {
		this.modifiedts = modifiedts;
	}

}
