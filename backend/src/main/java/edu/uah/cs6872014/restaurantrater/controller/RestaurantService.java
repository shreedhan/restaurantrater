/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.controller;

import java.sql.SQLException;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import edu.uah.cs6872014.restaurantrater.model.Restaurant;

/**
 * @author Suman
 *
 */

@Path("/restaurants")
public class RestaurantService {

	@GET
	@Path("/{rid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecord(@PathParam("rid") int rid) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("rid", Integer.toString(rid));

		StringBuilder sb = new StringBuilder();

		String result = null;
		try {
			result = Restaurant.jsonFindByKey(keyMap);
			if (result == null) {
				return Response.status(404).entity("Requested restaurant does not exist").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();
	}

	@GET
	@Path("/gid/{gid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecord(@PathParam("gid") String gid) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("gid", gid);

		StringBuilder sb = new StringBuilder();

		String result = null;
		try {
			result = Restaurant.jsonFindByKey(keyMap);
			if (result == null) {
				return Response.status(404).entity("Requested restaurant does not exist").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();
	}
	
	
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRecords() {
		StringBuilder sb = new StringBuilder();
		String result = null;
		try {
			result = Restaurant.jsonFindAll();
			if (result == null) {
				return Response.status(404).entity("No restaurants found").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertRecord(String jsonRestaurant) {
		System.out.println(jsonRestaurant);
		Restaurant restaurant = Restaurant.toRestaurant(jsonRestaurant);
		try {
			restaurant.save();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while inserting new record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(jsonRestaurant).build();
	}

	@PUT
	@Path("/{rid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateRecord(@PathParam("rid") int rid, String jsonRestaurant) {
		System.out.println(jsonRestaurant);
		Restaurant restaurant = Restaurant.toRestaurant(jsonRestaurant);
		try {
			restaurant.save();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while updating record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(restaurant.toJSON().toString()).build();
	}

	@DELETE
	@Path("/{rid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteRecord(@PathParam("rid") int rid) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("rid", Integer.toString(rid));
		Restaurant restaurant = null;
		String jsonRestaurant = null;
		try {
			restaurant = Restaurant.findByKey(keyMap);
			if (restaurant != null) {
				jsonRestaurant = restaurant.toJSON().toString();
				restaurant.delete();
			}
			else
				jsonRestaurant = new JSONObject().toString();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while deleting record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(jsonRestaurant).build();
	}
}
