package edu.uah.cs6872014.restaurantrater.controller;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

import edu.uah.cs6872014.restaurantrater.model.Favorite;
import edu.uah.cs6872014.restaurantrater.model.Restaurant;

/**
 * @author Madhu Sigdel
 * 
 */

@Path("/favorites")
public class FavoriteService {

	@GET
	@Path("/{uid}&{rid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecord(@PathParam("uid") int uid, @PathParam("rid") int rid) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("uid", Integer.toString(uid));
		keyMap.put("rid", Integer.toString(rid));

		StringBuilder sb = new StringBuilder();

		String result = null;
		try {
			result = Favorite.jsonFindByKey(keyMap);
			if (result == null) {
				return Response.status(404).entity("Requested favorite data does not exist").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();

	}

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllRecords() {

		StringBuilder sb = new StringBuilder();

		String result = null;
		try {
			result = Favorite.jsonFindAll();
			if (result == null) {
				return Response.status(404).entity("No favorite data found").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();

	}

	@GET
	@Path("/{uid}&")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllUserRecord(@PathParam("uid") int uid) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("uid", Integer.toString(uid));

		StringBuilder sb = new StringBuilder();

		String result = null;
		try {
			result = Favorite.jsonFindByKeyMap(keyMap);
			if (result == null) {
				return Response.status(404).entity("Requested favorite data does not exist").build();
			}
		}
		catch (SQLException e) {
			sb.append(e.getMessage());
			e.printStackTrace();
		}
		sb.append(result);

		return Response.status(200).entity(sb.toString()).build();

	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertRecord(String jsonFavorite) {
		System.out.println(jsonFavorite);
		JSONObject favObj = new JSONObject(jsonFavorite);
		int uid = favObj.getInt("uid");
		String gid = favObj.getString("gid");
		String restaurantName = favObj.getString("restName");
		String restaurantAddress = favObj.getString("restAddress");
		double lat = favObj.getDouble("lat");
		double lng = favObj.getDouble("lng");
		int restaurantZipCode = favObj.getInt("restZip");

		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("gid", gid);
		Restaurant restaurant = null;
		try {
			restaurant = Restaurant.findByKey(keyMap);
			if (restaurant == null) {
				restaurant = new Restaurant();
				restaurant.setGid(gid);
				restaurant.setName(restaurantName);
				restaurant.setStaddress(restaurantAddress);
				restaurant.setZipcode(restaurantZipCode);
				restaurant.setLatitude(lat);
				restaurant.setLongitude(lng);
				restaurant = restaurant.save();
			}
		}
		catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			restaurant = null;
		}

		if (restaurant == null)
			return Response.status(500).entity("Cannot create a restaurant record\n").build();
		Favorite favorite = new Favorite();
		favorite.setUid(uid);
		System.out.println("Rid: " + restaurant.getRid());
		favorite.setRid(restaurant.getRid());
		favorite.setCreatedts(Timestamp.valueOf(favObj.getString("createdts")));
		try {
			if (Favorite.findByKey(favorite.getPrimaryKey()) != null) {
				favorite.delete();
				favObj.put("status", "Deleted");
			}
			else {
				favorite.save();
				favObj.put("status", "Added");
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while inserting new record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(favObj.toString()).build();
	}

	@POST
	@Path("/gid")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response insertRecordUsingGID(String jsonFavorite) {
		System.out.println(jsonFavorite);
		JSONObject favObj = new JSONObject(jsonFavorite);
		String gid = favObj.getString("id");
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("gid", gid);
		Restaurant restaurant = null;
		try {
			restaurant = Restaurant.findByKey(keyMap);
			if (restaurant == null) {
				restaurant = new Restaurant();
				restaurant.setGid(gid);
				restaurant.save();
			}
			Favorite favorite = new Favorite();
			favorite.setRid(restaurant.getRid());
			favorite.setUid(favObj.getInt("uid"));

			favorite.save();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while inserting new record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(jsonFavorite).build();
	}

	@PUT
	@Path("/{uid}&{rid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateRecord(@PathParam("uid") int uid, @PathParam("rid") int rid, String jsonFavorite) {
		System.out.println(jsonFavorite);
		Favorite favorite = Favorite.toFavorite(jsonFavorite);
		try {
			favorite.save();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while updating record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(favorite.toJSON().toString()).build();
	}

	@DELETE
	@Path("/{uid}&{rid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteRecord(@PathParam("uid") int uid, @PathParam("rid") int rid) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("uid", Integer.toString(uid));
		keyMap.put("rid", Integer.toString(rid));
		Favorite favorite = null;
		String jsonFavorite = null;
		try {
			favorite = Favorite.findByKey(keyMap);
			if (favorite != null) {
				jsonFavorite = favorite.toJSON().toString();
				favorite.delete();
			}
			else
				jsonFavorite = new JSONObject().toString();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Exception while deleting record\n" + e.getMessage()).build();
		}
		return Response.status(200).entity(jsonFavorite).build();
	}

}
