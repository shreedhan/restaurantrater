/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uah.cs6872014.restaurantrater.dbinterface.MySQLDBConnection;
import edu.uah.cs6872014.restaurantrater.model.Comment;
import edu.uah.cs6872014.restaurantrater.model.User;

/**
 * @author Madhu Sigdel
 * 
 */

@Path("/rates/agg")
public class RateAggService {

	@GET
	@Path("/&{rid}&{cid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecord(@PathParam("rid") int rid, @PathParam("cid") int cid) throws JSONException, SQLException {

		String tableName = "RATE";
		String queryString = "SELECT RID, CID, AVG(STAR) AVGSTAR, COUNT(*) CNT FROM " + tableName + " WHERE RID = "
			+ rid + " AND CID = " + cid + " GROUP BY RID, CID";

		System.out.println(queryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement statement;
		try {
			statement = connection.prepareStatement(queryString);
			resultSet = statement.executeQuery();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Database not connected or Qyery Error").build();
		}

		String result = null;

		if (resultSet.next()) {

			double avgstar = 2;
			int count = 2;

			try {
				avgstar = resultSet.getDouble("avgstar");
				count = resultSet.getInt("cnt");
			}
			catch (SQLException e1) {
				e1.printStackTrace();
				System.out.println(Double.toString(avgstar));
				return Response.status(500).entity("SQL exception").build();
			}

			if (count > 0) {
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("rid", rid).put("cid", cid).put("avgstar", avgstar).put("count", count);
				result = jsonObj.toString();
			}
		}

		if (result == null) {
			return Response.status(404).entity("Requested rate does not exist").build();
		}
		System.out.println(result);
		return Response.status(200).entity(result).build();
	}

	@GET
	@Path("/group/{groupgid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecord(@PathParam("groupgid") String groupgid) throws JSONException, SQLException {

		String queryString = "SELECT GID, AVG(STAR) as AVGSTAR, COUNT(*) as CNT FROM RATE T INNER JOIN RESTAURANT R ON (R.RID = T.RID)  WHERE GID IN ("
			+ groupgid + ") GROUP BY GID";

		System.out.println(queryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement statement;
		try {
			statement = connection.prepareStatement(queryString);
			resultSet = statement.executeQuery();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Database not connected or Qyery Error").build();
		}

		ArrayList<JSONObject> rowlist = new ArrayList<JSONObject>();
		String result = null;

		while (resultSet.next()) {

			double avgstar = 0;
			int count = 0;
			String gid = null;

			try {
				gid = resultSet.getString("gid");
				avgstar = resultSet.getDouble("avgstar");
				count = resultSet.getInt("cnt");
			}
			catch (SQLException e1) {
				e1.printStackTrace();
				System.out.println(Double.toString(avgstar));
				return Response.status(500).entity("SQL exception").build();
			}

			if (count > 0) {
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("gid", gid).put("avgstar", avgstar).put("count", count);
				result = jsonObj.toString();
				rowlist.add(jsonObj);
			}
		}
		result = rowlist.toString();

		if (result == null) {
			return Response.status(404).entity("Requested rate does not exist").build();
		}

		return Response.status(200).entity(result).build();
	}

	// Rating of a particular restaurant by a particular user

	@GET
	@Path("/rid/{rid}/uid/{uid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRateOfRestaurantByUser(@PathParam("rid") int rid, @PathParam("uid") int uid)
		throws JSONException, SQLException {

		String queryString = "SELECT RATE.CID, RATE.RID, RATE.UID, RATE.STAR, CATEGORY.name, USER.fname, USER.midname, USER.lname "
			+ "FROM RATE, USER, CATEGORY "
			+ "WHERE RATE.RID = "
			+ rid
			+ " AND RATE.UID = "
			+ uid
			+ " AND RATE.CID = CATEGORY.CID AND RATE.UID = USER.UID " + "GROUP BY RATE.CID";

		System.out.println(queryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement statement;
		try {
			statement = connection.prepareStatement(queryString);
			resultSet = statement.executeQuery();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Database not connected or Qyery Error").build();
		}

		String result = null;

		while (resultSet.next()) {
			int restId = resultSet.getInt("rid");
			int catId = resultSet.getInt("cid");
			int userId = resultSet.getInt("uid");
			int star = resultSet.getInt("star");
			String category = resultSet.getString("name");
			String fname = resultSet.getString("fname");
			String lname = resultSet.getString("lname");
			String midname = resultSet.getString("midname");

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("rid", restId).put("uid", userId).put("cid", catId).put("star", star).put("category", category)
				.put("name", fname + midname + lname);
			result = jsonObj.toString();
		}

		if (result == null) {
			return Response.status(404).entity("Requested rate for the restaurant by the user does not exist").build();
		}

		return Response.status(200).entity(result).build();
	}

	// Rating and comments of a particular restaurant by all users
	@GET
	@Path("/rid/{rid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRateOfRestaurant(@PathParam("rid") int rid) throws JSONException, SQLException {

		String queryString = "SELECT RATE.CID, RATE.RID, RATE.UID, RATE.STAR, CATEGORY.name, USER.fname, USER.midname, USER.lname "
			+ "FROM RATE, USER, CATEGORY "
			+ "WHERE RATE.RID = "
			+ rid
			+ " AND RATE.CID = CATEGORY.CID AND RATE.UID = USER.UID ";

		System.out.println(queryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement statement;
		try {
			statement = connection.prepareStatement(queryString);
			resultSet = statement.executeQuery();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Database not connected or Qyery Error").build();
		}

		String result = null;
		HashMap<Integer, ArrayList<JSONObject>> rateMap = new HashMap<Integer, ArrayList<JSONObject>>();
		HashMap<Integer, String> userMap = new HashMap<Integer, String>();

		while (resultSet.next()) {
			int userId = resultSet.getInt("uid");
			int star = resultSet.getInt("star");
			String category = resultSet.getString("name");
			HashMap<String, String> userIDKey = new HashMap<String, String>();
			userIDKey.put("uid", Integer.toString(userId));
			User currentUser = User.findByKey(userIDKey);
			String fullName = currentUser.getFullName();
			JSONObject jsonObj = new JSONObject();
			jsonObj.put("star", star).put("category", category);
			if (!rateMap.containsKey(userId)) {
				ArrayList<JSONObject> list = new ArrayList<JSONObject>();
				list.add(jsonObj);
				rateMap.put(userId, list);
				userMap.put(userId, fullName);
			}
			else {
				ArrayList<JSONObject> list = rateMap.get(userId);
				list.add(jsonObj);
			}

		}

		JSONArray userRatings = new JSONArray();

		HashMap<Integer, Comment> commentMap = getCommentsOfRestaurant(rid);
		for (Map.Entry<Integer, ArrayList<JSONObject>> entry : rateMap.entrySet()) {
			JSONArray jsonArray = new JSONArray();
			int key = entry.getKey();
			ArrayList<JSONObject> list = entry.getValue();
			JSONObject userRatingObj = new JSONObject();
			for (JSONObject jsonObject : list) {
				jsonArray.put(jsonObject);
			}
			userRatingObj.put("rating", jsonArray);
			userRatingObj.put("fullname", userMap.get(key));
			if (commentMap != null && commentMap.containsKey(key)) {
				userRatingObj.put("comment", commentMap.get(key).getComment());
				userRatingObj.put("createdts", commentMap.get(key).getCreatedts());
				userRatingObj.put("modifiedts", commentMap.get(key).getModifiedts());
				commentMap.remove(key);
			}
			userRatings.put(userRatingObj);
		}

		// Add comments even if there are no ratings corresponding to these comments
		if (commentMap != null && commentMap.size() > 0) {
			for (Map.Entry<Integer, Comment> entry : commentMap.entrySet()) {
				JSONObject userRatingObject = new JSONObject();
				int uid = entry.getKey();
				HashMap<String, String> userKeyMap = new HashMap<String, String>();
				userKeyMap.put("uid", Integer.toString(uid));
				User user = User.findByKey(userKeyMap);
				String fullName = user.getFullName();
				System.out.println("fullname " + fullName);
				userRatingObject.put("fullname", fullName);
				userRatingObject.put("comment", entry.getValue().getComment());
				userRatingObject.put("createdts", entry.getValue().getCreatedts());
				userRatingObject.put("modifiedts", entry.getValue().getModifiedts());
				userRatingObject.put("rating", new JSONArray().toString());
				userRatings.put(userRatingObject);

			}
		}

		result = userRatings.toString();

		if (result == null) {
			return Response.status(404).entity("Requested rate for the restaurant by the user does not exist").build();
		}

		return Response.status(200).entity(result).build();
	}

	private HashMap<Integer, Comment> getCommentsOfRestaurant(int rid) {
		HashMap<String, String> keyMap = new HashMap<String, String>();
		keyMap.put("rid", Integer.toString(rid));
		HashMap<Integer, Comment> map = null;
		try {
			List<Comment> comments = Comment.findByKeyMap(keyMap);
			if (comments.size() > 0) {
				map = new HashMap<Integer, Comment>();
				for (Comment comment : comments) {
					map.put(comment.getUid(), comment);
				}

			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return map;
	}

	@GET
	@Path("/&{rid}&")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecord(@PathParam("rid") int rid) throws JSONException, SQLException {

		String tableName = "RATE";
		String queryString = "SELECT RID, AVG(STAR) as AVGSTAR, COUNT(*) as CNT FROM " + tableName + " WHERE RID = "
			+ rid + " GROUP BY RID";

		System.out.println(queryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement statement;
		try {
			statement = connection.prepareStatement(queryString);
			resultSet = statement.executeQuery();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Database not connected or Qyery Error").build();
		}
		String result = null;

		if (resultSet.next()) {

			double avgstar = 2;
			int count = 2;

			try {
				avgstar = resultSet.getDouble("avgstar");
				count = resultSet.getInt("cnt");
			}
			catch (SQLException e1) {
				e1.printStackTrace();
				System.out.println(Double.toString(avgstar));
				return Response.status(500).entity("SQL exception").build();
			}

			if (count > 0) {
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("rid", rid).put("avgstar", avgstar).put("count", count);
				result = jsonObj.toString();
			}
		}

		if (result == null) {
			return Response.status(404).entity("Requested rate does not exist").build();
		}

		return Response.status(200).entity(result).build();
	}

	@GET
	@Path("/gid/&{gid}&{cid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getgidRecord(@PathParam("gid") String gid, @PathParam("cid") int cid) throws JSONException,
		SQLException {

		String queryString = "SELECT GID, CID, AVG(STAR) AVGSTAR, COUNT(*) CNT FROM RATE T INNER JOIN RESTAURANT R ON ( R.RID = T.RID) WHERE GID = '"
			+ gid + "' AND CID = " + cid + " GROUP BY CID, GID";

		System.out.println(queryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement statement;
		try {
			statement = connection.prepareStatement(queryString);
			resultSet = statement.executeQuery();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Database not connected or Qyery Error").build();
		}

		String result = null;

		if (resultSet.next()) {

			double avgstar = 0;
			int count = 0;

			try {
				avgstar = resultSet.getDouble("avgstar");
				count = resultSet.getInt("cnt");
			}
			catch (SQLException e1) {
				e1.printStackTrace();
				System.out.println(Double.toString(avgstar));
				return Response.status(500).entity("SQL exception").build();
			}

			if (count > 0) {
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("gid", gid).put("cid", cid).put("avgstar", avgstar).put("count", count);
				result = jsonObj.toString();
			}
		}

		if (result == null) {
			return Response.status(404).entity("Requested rate does not exist").build();
		}

		return Response.status(200).entity(result).build();
	}

	@GET
	@Path("/gid/&{gid}&")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getgidRecord(@PathParam("gid") String gid) throws JSONException, SQLException {

		String queryString = "SELECT GID, AVG(STAR) as AVGSTAR, COUNT(*) as CNT FROM RATE T INNER JOIN RESTAURANT R ON (R.RID = T.RID)  WHERE GID = '"
			+ gid + "' GROUP BY GID";

		System.out.println(queryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement statement;
		try {
			statement = connection.prepareStatement(queryString);
			resultSet = statement.executeQuery();
		}
		catch (SQLException e) {
			e.printStackTrace();
			return Response.status(500).entity("Database not connected or Qyery Error").build();
		}
		String result = null;

		if (resultSet.next()) {

			double avgstar = 2;
			int count = 2;

			try {
				avgstar = resultSet.getDouble("avgstar");
				count = resultSet.getInt("cnt");
			}
			catch (SQLException e1) {
				e1.printStackTrace();
				System.out.println(Double.toString(avgstar));
				return Response.status(500).entity("SQL exception").build();
			}

			if (count > 0) {
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("gid", gid).put("avgstar", avgstar).put("count", count);
				result = jsonObj.toString();
			}
		}

		if (result == null) {
			return Response.status(404).entity("Requested rate does not exist").build();
		}

		return Response.status(200).entity(result).build();
	}

}
