/**
 * 
 */
package edu.uah.cs6872014.restaurantrater.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import edu.uah.cs6872014.restaurantrater.dbinterface.MySQLDBConnection;

/**
 * @author Suman
 * 
 */
public class Rate {

	private int				uid;
	private int				rid;
	private int				cid;
	private int				star;
	private Timestamp		createdts;
	private Timestamp		modifiedts;

	private static String	tableName	= "RATE";

	public Rate() {
	}

	public Rate(int uid, int rid, int cid) {
		this.uid = uid;
		this.rid = rid;
		this.cid = cid;
	}

	public static List<Rate> findAll() throws SQLException {
		String queryString = "SELECT * FROM " + tableName;
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		List<Rate> rowList = new ArrayList<Rate>();

		PreparedStatement statement = connection.prepareStatement(queryString);
		resultSet = statement.executeQuery();

		while (resultSet.next()) {
			Rate rate = new Rate();
			rate.uid = resultSet.getInt("uid");
			rate.rid = resultSet.getInt("rid");
			rate.cid = resultSet.getInt("cid");
			rate.star = resultSet.getInt("star");
			rate.createdts = resultSet.getTimestamp("createedts");
			rate.modifiedts = resultSet.getTimestamp("modifiedts");
			rowList.add(rate);
		}
		resultSet.close();
		return rowList;
	}

	public static List<Rate> findByKeyMap(Map<String, String> keyMap) throws SQLException {
		String queryString = "SELECT * FROM " + tableName + " WHERE 1 = 1";
		String queryCondition = "";
		if (keyMap.size() > 0) {
			for (Map.Entry<String, String> entry : keyMap.entrySet()) {
				queryCondition = queryCondition + " AND " + entry.getKey() + " = " + entry.getValue();
			}

			queryString = queryString + queryCondition;
		}
		System.out.println(queryString);
		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		List<Rate> rowList = new ArrayList<Rate>();

		PreparedStatement statement = connection.prepareStatement(queryString);
		resultSet = statement.executeQuery();

		while (resultSet.next()) {
			Rate rate = new Rate();
			rate.uid = resultSet.getInt("uid");
			rate.rid = resultSet.getInt("rid");
			rate.cid = resultSet.getInt("cid");
			rate.star = resultSet.getInt("star");
			rate.createdts = resultSet.getTimestamp("createedts");
			rate.modifiedts = resultSet.getTimestamp("modifiedts");
			rowList.add(rate);
		}
		resultSet.close();
		return rowList;
	}

	public static Rate findByKey(Map<String, String> keyMap) throws SQLException {
		String queryString = "SELECT * FROM " + tableName + " WHERE 1 = 1";
		String queryCondition = "";
		if (keyMap.size() > 0) {
			for (Map.Entry<String, String> entry : keyMap.entrySet()) {
				queryCondition = queryCondition + " AND " + entry.getKey() + " = " + entry.getValue();
			}

			queryString = queryString + queryCondition;
		}
		System.out.println(queryString);

		Connection connection = MySQLDBConnection.getConnection();
		ResultSet resultSet = null;
		PreparedStatement statement = connection.prepareStatement(queryString);
		resultSet = statement.executeQuery();
		if (resultSet.next()) {
			Rate rate = new Rate();
			rate.uid = Integer.parseInt(resultSet.getObject("uid").toString());
			rate.rid = Integer.parseInt(resultSet.getObject("rid").toString());
			rate.cid = Integer.parseInt(resultSet.getObject("cid").toString());
			rate.star = Integer.parseInt(resultSet.getObject("star").toString());
			rate.createdts = Timestamp.valueOf(resultSet.getObject("createdts").toString());
			rate.modifiedts = Timestamp.valueOf(resultSet.getObject("modifiedts").toString());
			return rate;
		}
		else
			return null;
	}

	public static String jsonFindByKey(HashMap<String, String> keyMap) throws SQLException {
		Rate rate = findByKey(keyMap);
		if (rate == null)
			return null;
		JSONObject jsonObj = rate.toJSON();
		return jsonObj.toString();
	}

	public static String jsonFindAll() throws SQLException {
		List<Rate> rates = findAll();
		JSONArray array = toJSONArray(rates);
		return array.toString();
	}

	public static String jsonFindByKeyMap(HashMap<String, String> keyMap) throws SQLException {
		List<Rate> rates = findByKeyMap(keyMap);
		JSONArray array = toJSONArray(rates);
		return array.toString();
	}

	public Map<String, String> getPrimaryKey() {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("uid", Integer.toString(this.uid));
		map.put("rid", Integer.toString(this.rid));
		map.put("cid", Integer.toString(this.cid));
		return map;
	}

	public Rate save() throws SQLException {
		Rate rate = findByKey(this.getPrimaryKey());
		if (rate == null) {
			String queryString = "INSERT INTO " + tableName + "(UID, RID, CID, STAR, CREATEDTS, MODIFIEDTS) VALUES ("
				+ this.uid + "," + this.rid + "," + this.cid + "," + this.star + ",'" + this.createdts + "','"
				+ this.modifiedts + "')";
			System.out.println(queryString);
			Connection connection = MySQLDBConnection.getConnection();
			int result = 0;
			PreparedStatement statement = connection.prepareStatement(queryString);
			result = statement.executeUpdate();
			if (result == 1) {
				System.out.println("Insert successfully");
			}
		}
		else {
			String queryString = "UPDATE " + tableName + " SET STAR = " + this.star + ", MODIFIEDTS = '"
				+ this.modifiedts + "' WHERE UID = " + this.uid + " AND RID = " + this.rid + " AND CID = " + this.cid;
			System.out.println(queryString);
			Connection connection = MySQLDBConnection.getConnection();
			int result = 0;
			PreparedStatement statement = connection.prepareStatement(queryString);
			result = statement.executeUpdate();
			if (result == 1) {
				System.out.println("Updated successfully");
			}
		}
		return this;
	}

	public Rate delete() throws SQLException {
		String queryString = "DELETE FROM " + tableName + " WHERE UID = " + this.uid + " AND RID = " + this.rid
			+ " AND CID = " + this.cid;
		System.out.println(queryString);
		Connection connection = MySQLDBConnection.getConnection();
		int result = 0;
		PreparedStatement statement = connection.prepareStatement(queryString);
		result = statement.executeUpdate();
		if (result == 1) {
			System.out.println("Deleted successfully");
		}
		return this;
	}

	public JSONObject toJSON() {
		JSONObject jsonObj = new JSONObject();
		jsonObj.put("uid", uid).put("rid", rid).put("cid", cid).put("star", star).put("createdts", createdts)
			.put("modifiedts", modifiedts);
		return jsonObj;
	}

	public static JSONArray toJSONArray(List<Rate> rates) {
		JSONArray array = new JSONArray();
		for (Rate rate : rates) {
			JSONObject jsonObj = rate.toJSON();
			array.put(jsonObj);
		}
		return array;
	}

	public static Rate toRate(String jsonRate) {
		Rate rate = new Rate();
		try {
			JSONObject jsonObj = new JSONObject(jsonRate);

			rate.uid = jsonObj.getInt("uid");
			rate.rid = jsonObj.getInt("rid");
			rate.cid = jsonObj.getInt("cid");
			rate.star = jsonObj.getInt("star");
			rate.createdts = Timestamp.valueOf(jsonObj.getString("createdts"));
			rate.modifiedts = Timestamp.valueOf(jsonObj.getString("modifiedts"));
		}
		catch (JSONException e) {
			System.out.println("Couldn't parse JSON for uid: " + rate.uid + ", rid : " + rate.rid + " & cid : "
				+ rate.cid);
			if (rate.uid <= 0 || rate.rid <= 0 || rate.cid <= 0)
				return null;
		}
		return rate;
	}

	/**
	 * @return the rid
	 */
	public int getRid() {
		return rid;
	}

	/**
	 * @param rid
	 *            the rid to set
	 */
	public void setRid(int rid) {
		this.rid = rid;
	}

	/**
	 * @return the cid
	 */
	public int getCid() {
		return cid;
	}

	/**
	 * @param cid
	 *            the cid to set
	 */
	public void setCid(int cid) {
		this.cid = cid;
	}

	/**
	 * @return the uid
	 */
	public int getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(int uid) {
		this.uid = uid;
	}

	/**
	 * @return the star
	 */
	public int getstar() {
		return star;
	}

	/**
	 * @param star
	 *            the star to set
	 */
	public void setstar(int star) {
		this.star = star;
	}

	/**
	 * @return the createdts
	 */
	public Timestamp getCreatedts() {
		return createdts;
	}

	/**
	 * @param createdts
	 *            the createdts to set
	 */
	public void setCreatedts(Timestamp createdts) {
		this.createdts = createdts;
	}

	/**
	 * @return the modifiedts
	 */
	public Timestamp getModifiedts() {
		return modifiedts;
	}

	/**
	 * @param modifiedts
	 *            the modifiedts to set
	 */
	public void setModifiedts(Timestamp modifiedts) {
		this.modifiedts = modifiedts;
	}

}
